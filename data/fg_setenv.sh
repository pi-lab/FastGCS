################################################################################
# FlightGear 3.2
################################################################################
FG_DIR=/home/bushuhui/research_data/uav/simulation/flightgear/flightgear-3.2

export FG_ROOT=$FG_DIR/data
export FG_SCENERY=$FG_ROOT/Scenery
export PATH=$FG_DIR/bin:$PATH
export MANPATH=$MANPATH:$FG_DIR/share/man

################################################################################
# OpenSceneGraph
################################################################################
export OSG_DIR=$FG_DIR/OpenSceneGraph-3.2.1

export LD_LIBRARY_PATH=$OSG_DIR/lib64:$LD_LIBRARY_PATH
export PKG_CONFIG_PATH=$OSG_DIR/lib64/pkgconfig:$PKG_CONFIG_PATH

