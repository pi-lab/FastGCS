#!/bin/sh

#FG_DIR=/usr/share/games/flightgear

#export LD_LIBRARY_PATH=$FG_DIR/lib:$LD_LIBRARY_PATH
#export FG_ROOT=$FG_DIR
#export FG_SCENERY=$FG_ROOT/Scenery:$FG_ROOT/WorldScenery

fgfs \
    --aircraft=arducopter \
    --fdm=network,localhost,5501,5502,5503 \
    --timeofday=noon \
    --fog-fastest \
    --disable-splash-screen \
    --disable-clouds \
    --disable-sound \
    --in-air \
    --enable-freeze \
    --airport=KSFO \
    --runway=10L \
    --altitude=7224 \
    --heading=113 \
    --offset-distance=4.72 \
    --offset-azimuth=0

#    --start-date-lat=2004:06:01:09:00:00 \


#fgfs --aircraft=HL20 --fdm=network,localhost,5501,5502,5503 --fog-fastest --disable-clouds --start-date-lat=2004:06:01:09:00:00 --disable-sound --in-air --enable-freeze --airport=KSFO --runway=10L --altitude=7224 --heading=113 --offset-distance=4.72 --offset-azimuth=0
