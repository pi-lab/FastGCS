# FastGCS

FastGCS是一个无人机地面站，这个程序最大的特点是集成了实时地图RTMapper、飞行仿真，通过这个地面站能够让无人机飞行过程实时生成飞行区域的地图。此外这个地面站优点是通过MAVLINK协议支持APM飞控，支持配置、任务规划等。

![FastGCS](./images/FastGCS4.png)

## 1. 编译、安装

本程序目前仅支持Linux操作系统，需要使用`Ubuntu 18.04` 及以上版本

### 1.1 需要的库和软件

```
sudo apt-get install build-essential cmake git
sudo apt install libunwind-dev binutils-dev

sudo apt install qt5-default qt5-qmake-bin qtdeclarative5-dev
sudo apt install libqt5network5 libqt5sql5 libqt5svg5 libqt5svg5-dev libqt5printsupport5

sudo apt-get install libpoco-dev libopencv-dev
```

### 1.2 编译
下载代码
```
git clone git@gitee.com:pi-lab/FastGCS.git
```

编译
```
# 进入代码目录
cd FastGCS

# 编译
mkdir build
cd build
cmake ..
make -j10
```

编译完成之后，可执行在`./bin/FastGCS`

## 2. 运行

本软件需要一些配置才能运行，所以需要在`./data`目录下运行程序，执行下面的命令

```
# 进入data目录并执行
cd ../data
cp ../bin/FastGCS .
./FastGCS
```

### 2.1 飞行仿真

**旋翼机飞行仿真**

Step1：打开虚拟飞机，`File`-`Open`，选择虚拟飞机的多旋翼无人机

Step2：设置航线

Step3：发送航点信息到无人机，`Test` - `Send Mission to VUAV`

Step4：执行虚拟飞行，`Test` - `Start Mission`



**固定翼无人机仿真**

Step0：连接飞行摇杆

Step1：打开虚拟飞机，`File`-`Open`，选择虚拟飞机的多旋翼无人机

Step2：设置航线

Step3：发送航点信息到无人机，`Test` - `Send Mission to VUAV`

Step4：对于ThrustMaster遥感，点击`SE`按钮，启动飞机

Step5：手动操控飞机起飞（自动起飞还没有实现）

Step6：飞行到一定安全高度，点击菜单`Test` - `Start Mission`



## 参考资料

* [FastGCS操作使用手册](doc/FastGCS_manual.pptx)
* [FastGCS操作使用手册-图片](doc/FastGCS_manual_images.pptx)
* [FastGCS对应的无人机配置说明](doc/FastGCS_SystemDiagram.pptx)
