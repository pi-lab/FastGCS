#ifndef __POS_PLOT_H__
#define __POS_PLOT_H__

#include <string>

#include <QtCore>
#include <QtGui>
#include <QTimer>
#include <QGLViewer/qglviewer.h>

#include "base/osa/osa++.h"
#include "base/time/DateTime.h"
#include "gui/controls/qcustomplot.h"
#include "gui/mesh_off.h"


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

///
/// \brief The POS_Plot Base class
///
class POS_PlotBase
{
public:
    explicit POS_PlotBase();
    virtual ~POS_PlotBase();

    ///
    /// \brief Set Euler angles (yaw, pitch, roll)
    /// \param yaw_   - Yaw angle (in degree)
    /// \param pitch_ - Pitch angle (in degree)
    /// \param roll_  - Roll angle (in degree)
    /// \return
    ///
    virtual void setAttitude(double roll, double pitch, double yaw);

    virtual void addPoint(double x, double y, double z=0);
    virtual void clearPoint(void);

    virtual void setMessage(std::string &msg);

    virtual void plot(void) = 0;


protected:
    QVector<double>     m_arrPosX, m_arrPosY, m_arrPosZ;
    double              m_roll, m_pitch, m_yaw;
    double              m_x, m_y, m_z;

    pi::Mutex           m_mutex;
    std::string         m_szMsg;

    double              m_xMax, m_yMax, m_zMax;
};


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


///
/// \brief The GPS 2D Plot class
///
class POS_Plot2D : public QWidget, public POS_PlotBase
{
    Q_OBJECT

public:
    explicit POS_Plot2D(QWidget *parent = 0);
    virtual ~POS_Plot2D();


    virtual void addPoint(double x, double y, double z=0);
    virtual void clearPoint(void);


    void setupCanvas(void);
    void setupInitData(void);

    void setMarkerPos(double x, double y, double t, int id=0);
    void setMarkerSize(double s, int id=0);
    void setMarkerModel(double *parm, int id=0);

    void setPlotRange(double xmin, double xmax, double ymin, double ymax);

    void clear(void);
    virtual void plot(void);

    void setScreenShot_fname(std::string &fnBase);

    void showMessage(void);
    void showMessage(QString &msg);

    void resizeCanvas(void);

signals:
    void canvasReplot(void);

protected slots:
    void canvasReplot_slot(void);

protected slots:
    void canvsMousePressEvent(QMouseEvent *event);
    void canvasMouseMoveEvent(QMouseEvent *event);
    void canvasShowMessage(QString msg);

    void plotBegin(void);                   // slot for QCustomPlot begin plot
    void plotEnd(void);                     // slot for QCustomPlot end plot


protected:
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void resizeEvent(QResizeEvent *event);
    void timerEvent(QTimerEvent *event);

protected:
    QCustomPlot         *customPlot;

    double              parmMarkerModel[4];     // 0 - pos x
                                                // 1 - pos y
                                                // 2 - theta
                                                // 3 - size

    QCPCurve            *curvRealPos;
    QCPCurve            *curvMarker;

    QString             msgString1, msgString2;

    std::string         fnScreenShot_base;

    double              x_max, x_min, y_max, y_min;         // plot range
    double              markerSize, markerSizeRatio;        // UAV marker

private:
    void drawMarker(int idx=0);
};


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

///
/// \brief The GPS Plot 3D class
///
class POS_Plot3D : public QGLViewer, public POS_PlotBase
{
    Q_OBJECT

public :
    POS_Plot3D(QWidget *parent);
    virtual ~POS_Plot3D();

    virtual void addPoint(double x, double y, double z=0);
    virtual void clearPoint(void);

    virtual void plot(void);

    ///
    /// \brief Load a 3D model mesh
    /// \param fn_mesh - mesh file name
    /// \return
    ///
    int load_mesh(std::string &fn_mesh);

signals:
    void canvasReplot(void);

protected slots:
    void canvasReplot_slot(void);

protected:
    virtual void init();
    virtual void draw();

    virtual QString helpString() const;

    virtual void timerEvent(QTimerEvent *event);
    virtual void keyPressEvent(QKeyEvent *e);

protected:
    double                  m_sceneRadius;          // scene radius
    MeshData                m_mesh;                 // UAV model file name
};


#endif // end of __POS_PLOT_H__
