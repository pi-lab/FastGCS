#ifndef __UTILS_SYSTEM_H__
#define __UTILS_SYSTEM_H__

#include <deque>

#include "base/osa/osa++.h"

////////////////////////////////////////////////////////////////////////////////
/// run a givem program
////////////////////////////////////////////////////////////////////////////////
int run_program(const std::string &cmd);



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class RunProgram : public pi::Thread
{
public:
    RunProgram() { init(); }
    virtual ~RunProgram() { release(); }

    virtual int run(const std::string &cmd, int autoStart=1);
    virtual void stop(void);

    virtual void threadFunc();

    int init(void);
    int release(void);

protected:
    std::string     m_cmd;
    int             m_autoStart;

    void            *m_data;
};


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class MessageTTS : public pi::Thread
{
public:
    MessageTTS() { init(); }
    virtual ~MessageTTS() { release(); }

    virtual int run(void);
    virtual void stop(void);

    virtual void threadFunc();

    virtual int init(void);
    virtual int release(void);


protected:
    std::string                 m_cmdESpeak, m_cmdWavPlayer;
    std::string                 m_soundDev;

    int                         m_isRunning;
    pi::Mutex                   m_mutex;

    std::deque<std::string>     m_msgQueue;


public:
    virtual int pushMessage(const std::string &msg) {
        pi::ScopedMutex m(m_mutex);
        m_msgQueue.push_back(msg);
        return 0;
    }

    virtual int popMessage(std::string &msg) {
        pi::ScopedMutex m(m_mutex);

        msg.clear();
        if( m_msgQueue.size() > 0 ) {
            msg = m_msgQueue.front();
            m_msgQueue.pop_front();
            return 0;
        }

        return -1;
    }
};


#endif // end of __UTILS_SYSTEM_H__
