#ifndef __DATAMANAGER_H__
#define __DATAMANAGER_H__

#include <string>

#include <base/types/SPtr.h>
#include <cv/highgui/VideoPOS_Manager.h>
#include <hardware/Joystick/HAL_JSCombo.h>

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class ComManager;
class VideoManager;
class UAS_Manager;
class VirtualUAV_Manager;


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class DataManager
{
public:
    enum InputDataType {
        DATA_NONE,                      ///< not opened
        DATA_FILE,                      ///< saved file
        DATA_UDP,                       ///< UDP
        DATA_SIMULATION,                ///< Simulation
        DATA_LIVE                       ///< live capture
    };

public:
    DataManager(std::string systemName="FastGCS");
    virtual ~DataManager();

    virtual int open(InputDataType dt, std::string fn, int param);
    virtual int close(void);
    virtual int seek(double dt);

    virtual int runSLAM(void);
    virtual int stopSLAM(void);

    ComManager*             getComManager(void) { return m_comManager.get(); }
    UAS_Manager*            getUASManager(void) { return m_uasManager.get(); }
    VirtualUAV_Manager*     getVUAVManager(void) { return m_vuavManager.get(); }
    pi::VideoPOS_Manager*   getVideoPOS_Manager(void) { return m_videoPOS_Manager.get(); }
    pi::HAL_JSComobo*       getJoystick(void) { return m_joystick.get(); }

protected:
    SPtr<ComManager>            m_comManager;                   ///< communication
    SPtr<UAS_Manager>           m_uasManager;                   ///< UAS manager
    SPtr<VirtualUAV_Manager>    m_vuavManager;                  ///< virtual UAV manager
    SPtr<pi::VideoPOS_Manager>  m_videoPOS_Manager;             ///< VideoPOS manager
    SPtr<pi::HAL_JSComobo>      m_joystick;                     ///< Joystick

    std::string                 m_systemName;

    InputDataType               m_dataType;                     ///< input data type
    int                         m_isProjectOpened;              ///< project is open
};


#endif // end of __DATAMANAGER_H__
