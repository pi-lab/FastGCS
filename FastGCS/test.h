#pragma once

#include <base/Svar/ParamArray.h>

int test_mavlink_reading(pi::CParamArray *pa);
int test_GCP_DataAnalysis(pi::CParamArray *pa);

int test_uart(pi::CParamArray *pa);
int test_gps(pi::CParamArray *pa);

int test_FlightData_Recv(pi::CParamArray *pa);
int conv_FlightData(pi::CParamArray *pa);

int test_MissionData_Trans_server(pi::CParamArray *pa);
int test_MissionData_Trans_client(pi::CParamArray *pa);

int test_VirtualUAV(pi::CParamArray *pa);
int test_mobileGPS(pi::CParamArray *pa);

int test_plugin(pi::CParamArray *pa);


