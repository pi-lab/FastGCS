#ifndef __UTILS_COMMANAGER_H__
#define __UTILS_COMMANAGER_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string>

#include "base/osa/osa++.h"
#include "hardware/UART/UART.h"
#include "uav/FlightData_Transfer.h"
#include "uav/MissionData_Transfer.h"
#include "uav/UAS.h"
#include "uav/FlightGear_Interface.h"


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class UAS_Manager;


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class ComManager : public pi::Thread
{
public:
    enum ComDeviceType {
        COM_DEVICE_NONE,
        COM_DEVICE_UART,
        COM_DEVICE_UDP,
        COM_DEVICE_IPC,
        COM_DEVICE_FILE
    };

    enum ComDeviceStatus {
        COM_DEVICE_CLOSED,
        COM_DEVICE_OPEND
    };

public:
    ComManager();
    virtual ~ComManager();

    virtual void threadFunc();

    virtual int open(ComDeviceType t, std::string devName, int baud=115200);
    virtual int close(void);

    virtual int parseMsg(mavlink_message_t *msg);

    virtual int seek(double dt);

    virtual int read(uint8_t *buf, int len);
    virtual int write(uint8_t *buf, int len);

    uint64_t getReadBytes(void) { return m_nRead; }
    uint64_t getWriteBytes(void) { return m_nWrite; }

    void         setUASManager(UAS_Manager *u) { m_uasManager = u; }
    UAS_Manager* getUASManager(void) { return m_uasManager; }

    pi::UART*               getUART(void) { return m_uart; }
    FlightData_Transfer*    getFlightData_Transfer(void) { return &m_fdTrans; }
    FlightGear_Transfer*    getFlightGear_Transfer(void) { return &m_fgTrans; }

    ComDeviceStatus getDeviceStatus(void) { return m_deviceStatus; }
    ComDeviceType   getDeviceType(void) { return m_deviceType; }

    void setAutoSavePath(std::string s) { m_autoSavePath = s; }

protected:
    void keepPlaySpeed(void);
    void syncTimeStamp(void);

protected:
    UAS_Manager             *m_uasManager;                  ///< UAS manager
    pi::UART                *m_uart;                        ///< UART obj

    FlightData_Transfer     m_fdTrans;                      ///< flight data transfer
    MissionData_Transfer    m_mdTrans;                      ///< mission data transfer
    MissionData_Transfer    m_mdTransServer;                ///< mission data transfer (server)
    FlightGear_Transfer     m_fgTrans;                      ///< flight gear data transfer

    uint64_t                m_nRead, m_nWrite;

    ComDeviceType           m_deviceType;
    ComDeviceStatus         m_deviceStatus;

    std::string             m_autoSavePath;
    std::string             m_autoSaveFileName;
    FILE                    *m_autoSaveFP;

    pi::ri64                m_tm0, m_tm1;
    pi::ri64                m_st0, m_st1;
    pi::ri64                m_nLastPOS;

    pi::ri64                m_tsValid;

    double                  m_playFF;                       ///< fast forward to


    int                     *m_mavlinkMsgShow;              ///< global configure - show/hide mavlink message
    double                  *m_playSpeed;                   ///< global configure - play speed (0.1 ~ 10x)
    double                  *m_playTS;                      ///< global configure - current play timestamp (unit second)
};

#endif // end of __UTILS_COMMANAGER_H__
