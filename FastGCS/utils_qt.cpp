#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

#include <base/Svar/Svar.h>
#include <base/utils/utils_str.h>

#include "utils_qt.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

QT_OS_Type getOS(void)
{
#ifdef Q_WS_X11
    return QT_OS_LINUX;
#endif

#ifdef Q_WS_WIN
    return QT_OS_WIN;
#endif

#ifdef Q_WS_MACX
    return QT_OS_MAC;
#endif
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

QStringList getComDevices(void)
{
    QT_OS_Type ost;
    QStringList devList;

    // get OS type
    ost = getOS();

    // Linux
    if( ost == QT_OS_LINUX ) {
        QDir d("/dev/");
        d.setFilter(QDir::System);

        QFileInfoList list = d.entryInfoList();
        for (int i = 0; i < list.size(); ++i) {
            QFileInfo fileInfo = list.at(i);
            QString fn = fileInfo.fileName();

            if( fn.contains("tty") ) {
                if( fn.contains("USB") || fn.contains("ACM") ) {
                    QString devName = d.absoluteFilePath(fn);

                    devList.push_back(devName);
                }
            }
        }
    }

    // Network connections
    {
        pi::StringArray sa;
        std::string devNetwork= svar.GetString("FastGCS.COM.Network", "TCP:192.168.0.99:6789");

        sa = pi::split_text(devNetwork, " ");

        for(int i=0; i<sa.size(); i++) {
            devList.push_back(QString::fromStdString(sa[i]));
        }
    }


    return devList;
}
