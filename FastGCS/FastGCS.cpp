/*******************************************************************************

  Robot Toolkit ++ (RTK++)

  Copyright (c) 2007-2014 Shuhui Bu <bushuhui@nwpu.edu.cn>
    http://www.adv-ci.com

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>

#include <QtCore>
#include <QtGui>
#include <QApplication>

#include <base/utils/utils.h>
#include <uav/UAS.h>

#include "DataManager.h"
#include "GCS_MainWindow.h"
#include "test.h"

using namespace std;
using namespace pi;


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int FastGCS(CParamArray *pa)
{
    int     argc;
    char**  argv;

    string  port = "/dev/ttyUSB0";
    int     port_type = 0;
    int     baud = 115200;

    string  fn_conf = "./Data/FastGCS/FastGCS.ini";

    DataManager dataManager("FastGCS");

    // parse input arguments
    argc = svar.i["argc"];
    argv = (char**) SvarWithType<void*>::instance()["argv"];

    // begin Qt
    QApplication app(argc, argv);

    // create setting object
    fn_conf = svar.GetString("FastGCS.Qt.configFN", fn_conf);
    QSettings *conf = new QSettings(QString::fromStdString(fn_conf), QSettings::IniFormat);
    svar.GetPointer("Qt.settings.ptr", NULL) = conf;

    // create main window
    GCS_MainWindow gcs(NULL);

    gcs.setDataManager(&dataManager);
    gcs.show();

    // open DataManager
    if( 1 ) {
        port      = svar.GetString("port", port);
        baud      = svar.GetInt("baud", baud);
        port_type = svar.GetInt("port_type", port_type);

        if( port_type == 0 )
            dataManager.open(DataManager::DATA_LIVE, port, baud);
        else
            dataManager.open(DataManager::DATA_FILE, port, baud);
    }

    // begin Qt thread
    app.exec();

    // close system
    dataManager.close();

    // delete qconfigure
    delete conf;

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

struct TestFunctionArray g_fa[] =
{
    TEST_FUNC_DEF(test_uart,                "Test UART"),

    TEST_FUNC_DEF(test_mavlink_reading,     "Test MAVLINK reading & parsing"),
    TEST_FUNC_DEF(test_gps,                 "Test GPS coordinate calculation"),

    TEST_FUNC_DEF(test_GCP_DataAnalysis,    "GCP data analysis"),

    TEST_FUNC_DEF(conv_FlightData,          "Convert to FlightData"),
    TEST_FUNC_DEF(test_FlightData_Recv,     "Test FlightData transmitter"),

    TEST_FUNC_DEF(test_MissionData_Trans_server, "Test MissionData_Trans server"),
    TEST_FUNC_DEF(test_MissionData_Trans_client, "Test MissionData_Trans client"),

    TEST_FUNC_DEF(test_VirtualUAV,          "Test flight simulation"),

    TEST_FUNC_DEF(test_mobileGPS,           "Test mobile GPS"),

    TEST_FUNC_DEF(test_plugin,              "Test plugin, dynamically load share libraries"),

    TEST_FUNC_DEF(FastGCS,                  "FastGCS"),

    {NULL,  "NULL",  "NULL"},
};


int main(int argc, char *argv[])
{
    // run function
    return svar_main(argc, argv, g_fa);
}

