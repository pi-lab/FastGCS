#ifndef __GCS_MAINWINDOW_H__
#define __GCS_MAINWINDOW_H__

#include <string>

#include <QtCore>
#include <QtGui>
#include <QMainWindow>
#include <QTimer>
#include <QGLViewer/qglviewer.h>

#include <base/osa/osa++.h>
#include <base/utils/utils.h>
#include <gui/controls/qcustomplot.h>
#include <gui/controls/qFlightInstruments.h>
#include <gui/gl/Win3D.h>
#include <uav/UAS.h>

#include "AHRS_GLViewer.h"
#include "POS_plot.h"
#include "MapWidget.h"

#include "ComManager.h"
#include "DataManager.h"


///
/// \brief The GCS_MainWindow class
///
class GCS_MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit GCS_MainWindow(QWidget *parent = 0);
    virtual ~GCS_MainWindow();

    virtual int setupLayout(void);

    // set/get Data Manager
    virtual int setDataManager(DataManager *dm);
    virtual DataManager* getDataManager(void) { return m_dataManager; }

    // set/get UAS manager
    virtual int setUASManager(UAS_Manager *u);
    virtual UAS_Manager* getUASManager(void) { return m_uasManager; }

    // set/get ComManager
    virtual int setComManager(ComManager *cm);
    virtual ComManager* getComManager(void) { return m_comManager; }

    // AHRS_GLViewer functions
    int loadMesh(void);

    // get action pointer by name
    QAction* getAction(const QString &actName);

protected:
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void resizeEvent(QResizeEvent *event);
    void timerEvent(QTimerEvent *event);
    void closeEvent(QCloseEvent *event);

protected:
    void createActions(void);
    void createToolBars(void);
    void createMainMenu(void);

protected:
    // right-panel
    QDockWidget                 *m_flightInfoPanel;
    QTabWidget                  *m_tabFlightInfoControl;
    pi::QFlightIns              *m_FlightIns;
    pi::QKeyValueListView       *m_infoList;
    UI_flightControlPanel       *m_flightControl;

    // left-pannel
    QTabWidget                  *m_tabWidget;
    MapWidget                   *m_mapView;
    pi::gl::Win3D               *m_viewSLAM;
    POS_Plot2D                  *m_plot2D;
    POS_Plot3D                  *m_plot3D;
    AHRS_GLViewer               *m_ahrsViewer;

    // status-bar message
    QString                     m_sbTitleString;
    QString                     m_sbString1, m_sbString2;

    // data, UAS, communication managers
    DataManager                 *m_dataManager;
    UAS_Manager                 *m_uasManager;
    ComManager                  *m_comManager;

    // other
    pi::Mutex                   m_mutex;
    QSettings                   *m_conf;

    // GUI objs
    QMap<QString, QAction*>     m_actionMap;
    QVector<QMenu*>             m_menuList;
    QToolBar                    *menuToolBar;

    // splash screen
    int                         m_splashScreen;
    int                         m_splashScreenTS;

    // system refresh interval
    double                      m_systemRefreshInterval;

public slots:
    void    action_Open(void);
    void    action_Close(void);
    void    action_Quit(void);

    void    action_SvarEdit(void);

    void    action_ShowFullScreen(void);
    void    action_ShowHideStatusBar(void);
    void    action_ShowHideMenuBar(void);
    void    action_ShowHideFlightInfoPanel(void);
    void    action_ShowMsgBoard(void);
    void    action_ShowStatusText(void);
    void    action_ShowLogInfo(void);
    void    action_ShowVideoFrame(void);
    void    action_VideoFrameScaleUp(void);
    void    action_VideoFrameScaleDown(void);
    void    action_PlaySpeed(void);
    void    action_ViewTab_Map(void);
    void    action_ViewTab_SLAM(void);
    void    action_ShowHideGuidedTarget(void);
    void    action_ViewFastForward(void);

    void    action_ClearPos(void);
    void    action_loadMAVParameters(void);
    void    action_uploadWaypoints(void);
    void    action_downloadWaypoints(void);
    void    action_clearWaypoints(void);
    void    action_setCurrentWP(void);
    void    action_preFlightCalibration(void);
    void    action_flightModeConfig(void);
    void    action_rcConfig(void);
    void    action_accelConfig(void);
    void    action_compassConfig(void);
    void    action_setGimbalPitch(void);
    void    action_setServo(void);
    void    action_landingGearDown(void);
    void    action_landingGearUp(void);
    void    action_followMeStart(void);
    void    action_followMeStop(void);
    void    action_joystickStart(void);
    void    action_joystickStop(void);

    void    action_SLAM_Reset(void);
    void    action_SLAM_SaveKeyFrames(void);
    void    action_SLAM_SaveMap2D(void);
    void    action_SLAM_ViewNormal(void);
    void    action_SLAM_ViewSimple(void);
    void    action_SLAM_ViewFull(void);
    void    action_SLAM_ViewFineMap(void);
    void    action_SLAM_FollowCamera(void);
    void    action_SLAM_FitGPS(void);

    void    action_GCS_setGCS(void);
    void    action_GCS_setYawOffset(void);
    void    action_GCS_testATA(void);
    void    action_GCS_rcON(void);
    void    action_GCS_rcOFF(void);

    void    action_Test_saveVideo(void);
    void    action_Test_sendMission(void);
    void    action_Test_startMission(void);
    void    action_Test_stopMission(void);
    void    action_Test_execCommand(void);

    void    action_Help_about(void);

    void    mw_onMapDrag(void);
    void    mw_zoomChanged(int newZoom);
};

#endif // end of __GCS_MAINWINDOW_H__
