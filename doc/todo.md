# TODO

* [ ] Make a better installer program

* [ ] Check weather opmapcontrol can work under Windows
* [ ] Add Chinese UI to FGCS.
* [ ] Flight data playback with better play speed control.
* [ ] FGCS show link lost, but recevied data show link is correct.
* [ ] Save Map2DFusion results to file, save coordinate to file (GeoTIFF).
* [ ] Fix waypoint upload/download bug, missing last waypoint
* [ ] Fix Qt thread crash when log information are printed, make all MapWidget component update single in a main loop
* [ ] Add multi-MAV support
* [ ] Improve system structure to support different type flight controller
* [ ] Improve GUI system, make all actions into a single class
* [ ] Add description for APM parameters

* [x] Split PIS/PIL, make PIL as a submodule
* [x] Add battery remain power percentange display
* [x] Save received images to video file (selectable)
* [x] Fix bug in Thread and Socket++
* [x] Fix incorrect time when GPS is not locked
* [x] Fix bug in MessagePassing (pi::RMP_NodeMap::erase, pi::RMP_SocketThread::threadFunc)
* [x] Add waypoint download/confirm/upload function for AutoFlight
* [x] Add joystick to control guided flight
* [x] Add landing gear automatic down
* [x] add circular flight mode