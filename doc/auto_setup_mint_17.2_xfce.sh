#!/bin/bash

#
# download the Linux Mint 17.2 Xfce 64-bit at:
#   http://mirrors.ustc.edu.cn/linuxmint-cd//stable/17.2/linuxmint-17.2-xfce-64bit.iso
#


# add current user to dialout group
sudo usermod -a -G dialout $USER

# install command
install_command="sudo apt-get install -y"

################################################################################
# ia32 support librays
################################################################################
$install_command ia32-libs*

################################################################################
# chinese support
################################################################################
$install_command fcitx fcitx-bin fcitx-config-common fcitx-config-gtk fcitx-data fcitx-frontend-gtk2 fcitx-frontend-gtk3 fcitx-frontend-qt4 fcitx-googlepinyin fcitx-libs fcitx-module-dbus fcitx-module-x11 fcitx-modules fcitx-pinyin fcitx-table fcitx-table-wubi fcitx-ui-classic fcitx-libpinyin fcitx-module-cloudpinyin 
$install_command fcitx-sogoupinyin 

################################################################################
# building tools & librarys
################################################################################
$install_command vim-common vim-doc vim-gtk vim-scripts

$install_command build-essential
$install_command bin86 kernel-package 
$install_command g++ gcc
$install_command libstdc++5
$install_command libboost1.54-all-dev
    
$install_command exuberant-ctags cscope
$install_command rcs git
$install_command manpages-dev glibc-doc
$install_command manpages-posix manpages-posix-dev
$install_command ack-grep
$install_command cmake cmake-gui

$install_command libncurses5 libncurses5-dev
$install_command mesa-utils libglu1-mesa freeglut3 freeglut3-dev libglew-dev
$install_command libxmu-dev libxmu-headers

# gtk
$install_command libgtk2.0-dev

# install qt4
$install_command libqt4-core libqt4-dev libqt4-gui qt4-doc qt4-designer 
$install_command libqt4-opengl-dev libqtwebkit-dev
$install_command libqt4-qt3support libqwtplot3d-qt4-0 libqwtplot3d-qt4-dev qt4-dev-tools qt4-qtconfig libqt4-opengl-dev
$install_command qtcreator
$install_command libqglviewer-dev libqglviewer2

# install SDL
$install_command libsdl2-dev

# numerical calculation
$install_command libeigen3-dev libeigen3-doc
$install_command libsuitesparse-dev 

# other
$install_command lib3ds-dev
$install_command libgtk2.0-dev
$install_command libgtkglext1 libgtkglext1-dev
$install_command libgstreamer1.0-dev
$install_command libdc1394-22-dev
$install_command libv4l-dev qv4l2
$install_command libjpeg-dev
$install_command libpng12-dev

################################################################################
# utils
################################################################################

$install_command p7zip-full p7zip-rar unrar lbzip2 pigz

# archivement mount
$install_command libarchive-dev libfuse-dev libfuse2

# mplayer
$install_command mplayer mplayer-doc mencoder smplayer mplayer-fonts

# ffmpeg
$install_command ffmpeg
$install_command libavcodec54 libavcodec-dev libavdevice53 libavdevice-dev libavfilter3 libavfilter-dev libavformat54 libavformat-dev libavutil-dev libavutil52 libswscale-dev libswscale2 libavresample-dev

# exFAT
$install_command exfat-fuse

