/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef _UNDISTORTER_HPP_
#define _UNDISTORTER_HPP_

#include <opencv2/core/core.hpp>

#include "Camera.h"

class Undistorter
{
public:
    Undistorter(std::string config_file);
    Undistorter(Camera* in, Camera* out) : camera_in(in),camera_out(out) {
        prepareReMap();
    }
    ~Undistorter();

    bool undistort(const cv::Mat& image, cv::Mat& result);
    bool undistortFast(const cv::Mat& image, cv::Mat& result);

protected:
    bool prepareReMap();

public:
    Camera* camera_in;
    Camera* camera_out;

    int width_in, height_in;
    int width_out,height_out;

    float*  remapX;
    float*  remapY;
    int*    remapFast;

    int     *remapIdx;
    float   *remapCoef;

    /// Is true if the undistorter object is valid (has been initialized with
    /// a valid configuration)
    bool    valid;
};

#endif
