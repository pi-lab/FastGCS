/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef CAMERAS_H
#define CAMERAS_H

#include <base/types/types.h>
#include <base/types/SPtr.h>

namespace pi {
namespace hardware {

class CameraImpl;

class Camera
{
public:
    Camera(const std::string& name="");
    Camera(SPtr<CameraImpl>& Impl);

    static Camera createFromName(const std::string& name);

    std::string CameraType();

    std::string info();

    int applyScale(double scale=0.5);

    bool isValid();

    Point2d Project(const Point3d& p3d);

    Point3d UnProject(const Point2d& p2d);

    int width();

    int height();
private:
    SPtr<CameraImpl> impl;
};

}}

#endif // CAMERAS_H
