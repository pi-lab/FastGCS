/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef UNDISTORTER_H
#define UNDISTORTER_H

#include <opencv2/core/core.hpp>

#include "Cameras.h"

namespace pi{
namespace hardware{

class UndistorterImpl;

class Undistorter
{
public:
    Undistorter(Camera in, Camera out);

    bool undistort(const cv::Mat& image, cv::Mat& result);
    //Undistorting fast, no interpolate (bilinear) is used
    bool undistortFast(const cv::Mat& image, cv::Mat& result);

    Camera cameraIn();
    Camera cameraOut();

    bool prepareReMap();
    bool valid();
private:
    SPtr<UndistorterImpl> impl;
};

} // end of namespace hardware
} // end of namespace pi

#endif // UNDISTORTER_H
