Pilot Intelligence Library (PIL)
====================================================

1. Introduction

The PIL is a library for smart robot and UAV. It includes some usefull tools for C++ programming espesially in the areas of moblile robotics and computer vision.


2. Modules

PI_BASE         - basic of the PIL, including types, Svar, utils, system, osa, debug tools
PI_CV           - computer vision utils
PI_GUI          - GUI components, including Win3D, controls, etc.
PI_HARDWARE     - hardware component, drivers, including camera, GPS, IMU, Joystick, UART
PI_LUA          - Lua engine and utils
PI_NETWORK      - Network utils, including Socket++, MessagePassing, NetTransfer_UDP, utils
PI_UAV          - UAV utils


3. Dependency

Please install the list of required package list below.


4. Demo application

You can find the demonstration at './apps/DemoApp'. After the build, you can run the program by
    ../../bin/DemoApp 


################################################################################
# Required package:
################################################################################

sudo apt-get install -y build-essential
sudo apt-get install -y bin86 kernel-package 
sudo apt-get install -y g++ gcc
sudo apt-get install -y libboost1.54-all-dev
sudo apt-get install -y exuberant-ctags cscope
sudo apt-get install -y ack-grep
sudo apt-get install -y cmake cmake-gui
sudo apt-get install -y git

sudo apt-get install -y libncurses5 libncurses5-dev
sudo apt-get install -y mesa-utils libglu1-mesa freeglut3 freeglut3-dev 
sudo apt-get install -y libxmu-dev libxmu-headers
sudo apt-get install -y libcairo2 libcairo2-dev
sudo apt-get install -y libqt4-core libqt4-dev libqt4-gui qt4-doc qt4-designer 
sudo apt-get install -y libqt4-opengl-dev libqtwebkit-dev
sudo apt-get install -y libqt4-qt3support libqwtplot3d-qt4-0 libqwtplot3d-qt4-dev 
sudo apt-get install -y qt4-dev-tools qt4-qtconfig libqt4-opengl-dev
sudo apt-get install -y qtcreator
sudo apt-get install -y libqglviewer-dev libqglviewer2
sudo apt-get install -y lib3ds-dev
sudo apt-get install -y libsdl2-dev
sudo apt-get install -y libeigen3-dev libeigen3-doc
sudo apt-get install -y libsuitesparse-dev 
sudo apt-get install -y libgtk2.0-dev libgtkglext1 libgtkglext1-dev
sudo apt-get install -y libgstreamer1.0-dev libdc1394-22-dev libv4l-dev 
sudo apt-get install -y libjpeg-dev libpng12-dev 
sudo apt-get install -y ffmpeg
sudo apt-get install -y libavcodec54 libavcodec-dev libavdevice53 libavdevice-dev 
sudo apt-get install -y libavfilter3 libavfilter-dev libavformat54 libavformat-dev 
sudo apt-get install -y libavutil-dev libavutil52 libswscale-dev libswscale2 libavresample-dev

