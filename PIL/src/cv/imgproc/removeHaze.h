/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/


#ifndef __REMOVEHAZE_H__
#define __REMOVEHAZE_H__


#include <opencv2/core/core.hpp>

///
/// \brief remove haze of the image
///
/// \param src              - source image
/// \param dst              - resulting image
/// \param mfSize           - min filter size
/// \param gfSize           - guided filter size
/// \param minAtomsLight    - min atomsfer light value (220 default)
/// \return
///
int removeHaze(cv::Mat &src, cv::Mat &dst,
               int mfSize, int gfSize=0, int minAtomsLight=220);


#endif // end of __REMOVEHAZE_H__

