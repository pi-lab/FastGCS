/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>
#include <fstream>

//#define ENABLE_LOGGER

#ifdef ENABLE_LOGGER
#define LOG(C) Logger::instance()<<__FUNCTION__<<" "<<C<<std::endl;\
    Logger::instance().flush();
#else
#define LOG(C)
#endif

class Logger
{
public:
    Logger();
    static std::ofstream& instance()
    {
        static std::ofstream ofs("log.txt");
        return ofs;
    }
};

#endif // LOGGER_H
