/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef RANDOM_H
#define RANDOM_H

#include <cstdlib>

namespace pi {

class Random
{
public:
    Random();

    /**
     * Returns a random int in the range [min..max]
     * @param min
     * @param max
     * @return random int in [min..max]
     */
    static int RandomInt(int min, int max);

    /**
     * Returns a random number in the range [0..1]
     * @return random T number in [0..1]
     */
    template <class T>
    static T RandomValue(){
        return (T)rand()/(T)RAND_MAX;
    }

    /**
     * Returns a random number in the range [min..max]
     * @param min
     * @param max
     * @return random T number in [min..max]
     */
    template <class T>
    static T RandomValue(T min, T max){
        return Random::RandomValue<T>() * (max - min) + min;
    }

    /**
     * Returns a random number from a gaussian distribution
     * @param mean
     * @param sigma standard deviation
     */
    template <class T>
    static T RandomGaussianValue(T mean=0., T sigma=1.)
    {
    // Box-Muller transformation
    T x1, x2, w, y1;

    do {
      x1 = (T)2. * RandomValue<T>() - (T)1.;
      x2 = (T)2. * RandomValue<T>() - (T)1.;
      w = x1 * x1 + x2 * x2;
    } while ( w >= (T)1. || w == (T)0. );

    w = sqrt( ((T)-2.0 * log( w ) ) / w );
    y1 = x1 * w;

    return( mean + y1 * sigma );
    }

};

} //end of namespace

#endif // RANDOM_H

