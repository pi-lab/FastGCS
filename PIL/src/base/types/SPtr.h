/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/


#ifndef SPTR_H
#define SPTR_H

#define USE_TR1
//#define USE_BOOST

#ifdef USE_TR1

#include <tr1/memory>

#define SPtr std::tr1::shared_ptr
#define WPtr std::tr1::weak_ptr

#else

#ifdef USE_BOOST

#include <boost/shared_ptr.hpp>
#define SPtr boost::shared_ptr
#define WPtr boost::weak_ptr

#else

#include <tr1/memory>
template <class T>
struct SPtr:public std::tr1::shared_ptr<T>
{

}

#endif //USE_BOOST

#endif //USE_TR1

#endif // SPTR_H
