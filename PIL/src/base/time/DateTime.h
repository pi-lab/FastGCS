/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef __RTK_DATETIME_H__
#define __RTK_DATETIME_H__

#include <stdint.h>

#include <vector>
#include <ostream>

#include "base/Svar/DataStream.h"


namespace pi {


///
/// \brief The DateTime class
///
class DateTime
{
public:
    ///
    /// \brief The DateTime_Type enum
    ///
    enum DateTime_Type
    {
        DATETIME_LOCAL = 0,                 ///< Local time
        DATETIME_UTC,                       ///< UTC/GMT time
        DATETIME_GPS                        ///< GPS time (FIXME: not used yet)
    };


public:
    DateTime();
    DateTime(DateTime_Type dtt);
    DateTime(const DateTime &dt);
    DateTime(const char *sDT);
    DateTime(int32_t y, int32_t m, int32_t d);
    DateTime(int32_t h, int32_t min, int32_t s,
             int32_t ns);
    DateTime(int32_t y, int32_t m, int32_t d,
             int32_t h, int32_t min, int32_t s,
             int32_t ns=0);

    ~DateTime();

    ///
    /// \brief clear date/time
    /// \return
    ///
    int clear(void);

    ///
    /// \brief setDateTime by DateTime date
    /// \param dt - DateTime class data
    /// \return
    ///
    int setDateTime(const DateTime &dt);

    ///
    /// \brief setDate
    /// \param y    - year
    /// \param m    - month
    /// \param d    - day
    /// \param dtt  - date type
    /// \return
    ///
    int setDate(int32_t y, int32_t m, int32_t d,
                DateTime_Type dtt = DATETIME_LOCAL);

    ///
    /// \brief setTime
    /// \param h        - hour
    /// \param min_     - minute
    /// \param s        - second
    /// \param ns       - nano second
    /// \return
    ///
    int setTime(int32_t h, int32_t min_, int32_t s, int32_t ns=0);

    ///
    /// \brief setTime
    /// \param h        - hour
    /// \param min_     - minute
    /// \param fs       - second
    /// \return
    ///
    int setTime(int32_t h, int32_t min_, double fs);

    ///
    /// \brief setDateTime
    /// \param y        - year
    /// \param m        - month
    /// \param d        - day
    /// \param h        - hour
    /// \param min_     - minute
    /// \param s        - second
    /// \param ns       - nano second
    /// \param dtt      - date/time type (UTC, local)
    /// \return
    ///
    int setDateTime(int32_t y, int32_t m, int32_t d, int32_t h, int32_t min_, int32_t s,
                    int32_t ns=0,
                    DateTime_Type dtt = DATETIME_LOCAL);


    ///
    /// \brief set to current date
    /// \param dtt      - date/time type (UTC, local)
    /// \return
    ///
    int setCurrentDate(DateTime_Type dtt = DATETIME_LOCAL);

    ///
    /// \brief set to current time
    /// \param dtt      - date/time type (UTC, local)
    /// \return
    ///
    int setCurrentTime(DateTime_Type dtt = DATETIME_LOCAL);

    ///
    /// \brief set to current date and time
    /// \param dtt      - date/time type (UTC, local)
    /// \return
    ///
    int setCurrentDateTime(DateTime_Type dtt = DATETIME_LOCAL);


    ///
    /// \brief set time from string
    /// \param fmt      - string format
    /// \param sTime    - string contains time
    /// \return
    ///
    int timeFromString(const char *fmt, const char *sTime);

    ///
    /// \brief set date from string
    /// \param fmt      - string format
    /// \param sTime    - string contains date
    /// \return
    ///
    int dateFromString(const char *fmt, const char *sDate);

    ///
    /// \brief set date/time from string
    /// \param fmt          - string format
    /// \param sDateTime    - string contains date/time
    /// \return
    ///
    int dateTimeFromString(const char *fmt, const char *sDateTime);


    ///
    /// \brief to stdandard TM struct
    /// \param t        - tm
    /// \return
    ///
    const int toTM(struct tm *t) const;

    ///
    /// \brief toTime_t
    /// \return         - second since 1970/1/1
    ///
    const int64_t toTime_t(void) const;

    ///
    /// \brief setup date/time from time_t value
    /// \param t        - second since 1970/1/1
    /// \return
    ///
    int fromTime_t(int64_t t);

    ///
    /// \brief toTimeStampF
    /// \return         - second since 1970/1/1
    ///
    const double toTimeStampF(void) const;

    ///
    /// \brief toTimeStamp
    /// \return         - micro second since 1970/1/1
    ///
    const int64_t toTimeStamp(void) const;

    ///
    /// \brief setup date/time from time stamp
    /// \param ts       - second since 1970/1/1
    /// \return
    ///
    int fromTimeStampF(double ts);

    ///
    /// \brief setup date/time from time stamp
    /// \param ts       - micro second since 1970/1/1
    /// \return
    ///
    int fromTimeStamp(int64_t ts);




    ///
    /// \brief date/time to UCT
    /// \return
    ///
    DateTime& toUTC(void);

    ///
    /// \brief date/time to local
    /// \return
    ///
    DateTime& toLocalTime(void);


    ///
    /// \brief operator =
    /// \param other
    /// \return
    ///
    DateTime& operator = (const DateTime &other);

    bool operator != (const DateTime &other) const;
    bool operator == (const DateTime &other) const;
    bool operator <  (const DateTime &other) const;
    bool operator <= (const DateTime &other) const;
    bool operator >  (const DateTime &other) const;
    bool operator >= (const DateTime &other) const;

    friend std::ostream& operator << (std::ostream &os, const DateTime &dt);

    ///
    /// \brief diffTime - return the number of seconds elasped
    ///                     between t0 and current time
    /// \param t0 - input date/time of begin timing
    /// \return The seconds between t0 and date/time of this object
    ///
    double diffTime(DateTime &t0);

    ///
    /// \brief print to stdout
    ///
    void print(void);

    ///
    /// \brief read from file
    /// \param fp
    /// \return
    ///
    int read(FILE *fp);

    ///
    /// \brief write to file
    /// \param fp
    /// \return
    ///
    int write(FILE *fp);

    ///
    /// \brief toStream
    /// \param s - byte stream
    /// \return
    ///
    int toStream(RDataStream &s, int isSingleDS = 0);

    ///
    /// \brief fromStream
    /// \param s - byte stream
    /// \return
    ///
    int fromStream(RDataStream &s, int isSingleDS = 0);


public:
    int32_t         year;               ///< year
    int32_t         month;              ///< month
    int32_t         day;                ///< day of the month

    int32_t         hour;               ///< hour
    int32_t         min;                ///< minute
    int32_t         sec;                ///< second
    uint32_t        nano_sec;           ///< nano second

    int32_t         timeZone;       ///< time zone
    DateTime_Type   dt_type;        ///< Date/time type
};

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

///
/// \brief set_timeZone
///     Set current time zone from (-12 ~ 12)
///
/// \param tz - time zone different from UTC (unit: hour)
/// \return not used
///
/// \see get_timeZone
///
int set_timeZone(int tz);

///
/// \brief get_timeZone
///     Get current time zone
///
/// \return time zone (unit: hour)
///
int get_timeZone(void);

} // end of namespace pi

#endif // __RTK_DATETIME_H__

