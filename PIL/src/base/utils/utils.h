/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef __UTILS_H__
#define __UTILS_H__

#include "utils_misc.h"
#include "utils_str.h"
#include "utils_math.h"

// also include some usefully things
#include "base/types/types.h"
#include "base/time/Time.h"
#include "base/time/DateTime.h"
#include "base/debug/debug_config.h"


#endif // end of __UTILS_H__


