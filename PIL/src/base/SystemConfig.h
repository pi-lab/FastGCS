/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef __SYSTEMCONFIG_H__
#define __SYSTEMCONFIG_H__


#define PIL_ENDIANNESS_LITTLE           0       // little endian (Intel)
#define PIL_ENDIANNESS_BIG              1       // big endian (other)


#ifdef PIL_SYSTEM_LINUX_X86
#include "SystemConfig_Linux_X86.h"
#endif



#endif // end of __SYSTEMCONFIG_H__

