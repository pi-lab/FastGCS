/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef SCOMMAND_H
#define SCOMMAND_H

#include <vector>

#include "Svar.h"

#define scommand Scommand_getInstance()

///
/// ptr      - class pointer
/// sCommand - command string
/// sParams  - parameters
///
typedef void (*CallbackProc)(void* ptr, std::string sCommand, std::string sParams);


struct CallbackInfoStruct
{
    CallbackInfoStruct(CallbackProc callback,void* ptr):cbp(callback),thisptr(ptr){}

    void Call(std::string sCommand="", std::string sParams=""){cbp(thisptr, sCommand, sParams);}

    CallbackProc cbp;
    void* thisptr;
};

typedef std::vector<CallbackInfoStruct> CallbackVector;


class Scommand
{
public:
    Scommand();
    static Scommand& instance();

    void RegisterCommand(std::string sCommandName, CallbackProc callback, void* thisptr=NULL);
    void UnRegisterCommand(std::string sCommandName);
    void UnRegisterCommand(std::string sCommandName,void* thisptr);
    void UnRegisterCommand(void* thisptr);

    bool Call(std::string sCommand, std::string sParams);
    bool Call(const std::string& sCommand);

protected:
    SvarWithType<CallbackVector>    &data;
};

Scommand& Scommand_getInstance(void);

#endif // SCOMMAND_H
