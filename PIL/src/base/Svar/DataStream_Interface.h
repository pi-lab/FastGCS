/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef DATASTREAM_H
#define DATASTREAM_H

class DataStream;

class StreamAble
{
public:
    //Success return 0
    virtual int   toStream(const DataStream& stream)=0;
    virtual int fromStream(const DataStream& stream)=0;
};

template <class T>
class IsStreamAble
{
    enum {Result=false};
};

class DataStream
{
public:
    DataStream();

    DataStream& operator <<(const StreamAble& element);
    int add(const StreamAble* ele);

protected:

};

#endif // DATASTREAM_H
