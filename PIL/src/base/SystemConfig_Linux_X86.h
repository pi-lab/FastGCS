/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/


#ifndef __SYSTEMCONFIG_LINUX_X86_H__
#define __SYSTEMCONFIG_LINUX_X86_H__

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef PIL_LINUX
    #define PIL_LINUX                           // for Linux
#endif

/*
#ifndef PIL_WINDOWS
    #define PIL_WINDOWS                         // for Windows
#endif
*/

/*
#ifndef PIL_MINGW
    #define PIL_MINGW                           // for MinGW
#endif
*/


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/*
#ifndef PIL_DEBUG
    #define PIL_DEBUG                           // debug switch open or cose
#endif
*/


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define PIL_ENDIANNESS  PIL_ENDIANNESS_LITTLE   // X86 is little endian


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/*
#ifndef USE_C
    #define USE_C                               // compile some source use C
#endif
*/


/*
#ifndef HAVE_SSE2
    #define HAVE_SSE2                           // dSFMT use SSE2 instructions
#endif
*/

/*
#ifndef HAVE_OPENMP
    #define HAVE_OPENMP                         // use OpenMP
#endif
*/


#ifndef HAS_OPENCV
    #define HAS_OPENCV                          // use OpenCV
#endif


#endif // end of __SYSTEMCONFIG_LINUX_X86_H__

