/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/


#ifndef SYSTEM_BASE_H
#define SYSTEM_BASE_H

#include <base/osa/osa++.h>
#include "MainWindowBase.h"

namespace pi
{


class SystemBase : public Thread, public EventHandle
{
public:
    SystemBase();
    virtual ~SystemBase();
    virtual void initialize();

    virtual void threadFunc();



protected:
    MainWindowBase* mWindow;
    int &           pause;//svar.i["SystemPause"]
};


}
#endif // SYSTEM_BASE_H
