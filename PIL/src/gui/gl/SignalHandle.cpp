/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#include <GL/glew.h>

#include "SignalHandle.h"

namespace pi{
namespace gl{

void Signal_Handle::delete_buffer_slot(GLuint buffer)
{
    glDeleteBuffers(1, &buffer);
}

Signal_Handle& Signal_Handle::instance()
{
    static Signal_Handle* sig=NULL;
    if(sig)
    {
        return *sig;
    }
    else
    {
        sig=new Signal_Handle();
        return *sig;
    }
}

}
}
