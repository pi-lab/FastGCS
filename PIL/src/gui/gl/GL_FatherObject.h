/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef GL_FATHEROBJECT_H
#define GL_FATHEROBJECT_H

#include "GL_Object.h"
#include <base/types/SPtr.h>
#include <base/osa/osa++.h>
#include <vector>

namespace pi{
namespace gl{

typedef SPtr<GL_Object>           GL_ObjectPtr;
typedef std::vector<GL_Object>    ObjectVec;
typedef std::vector<GL_ObjectPtr> ObjectPtrVec;

struct Father_Object : public GL_Object
{
    virtual void draw()
    {
        pi::ScopedMutex lock(m_mutex);
        for(ObjectVec::iterator it=children.begin();it!=children.end();it++)
            it->draw();
        for(ObjectPtrVec::iterator it=ptr_children.begin();it!=ptr_children.end();it++)
            (*it)->draw();
    }

    bool insert(const GL_ObjectPtr& obj){pi::ScopedMutex lock(m_mutex);ptr_children.push_back(obj);}
    bool insert(const GL_Object& obj){pi::ScopedMutex lock(m_mutex);children.push_back(obj);}

    void clear(){pi::ScopedMutex lock(m_mutex);children.clear();ptr_children.clear();}

private:
    ObjectVec       children;
    ObjectPtrVec    ptr_children;
    Mutex           m_mutex;
};

}}
#endif // GL_FATHEROBJECT_H
