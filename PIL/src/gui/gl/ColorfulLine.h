/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef COLORFULLINE_H
#define COLORFULLINE_H

#include "ColorfulPoint.h"

namespace pi{
namespace gl{

class ColorfulLine: public GL_Object
{
public:
    ColorfulLine(const std::vector<ColorfulPoint>& pts=std::vector<ColorfulPoint>())
        :points(pts){}

    virtual void draw()
    {
        if(points.size()>=2)
        {
            glBegin(GL_LINE);
            for(size_t i=0,iend=points.size();i<iend;i++)
            {
                glColor3b(points[i].color.x,points[i].color.y,points[i].color.z);
                glVertex3f(points[i].point.x,points[i].point.y,points[i].point.z);
            }
            glEnd();
        }
    }

private:
    std::vector<ColorfulPoint> points;
};

}
}
#endif // COLORFULLINE_H
