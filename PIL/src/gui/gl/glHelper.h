/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef GLHELPER_H
#define GLHELPER_H

#include <base/types/SE3.h>

void glVertex(const pi::Point3d& pt);
void glVertex(const pi::Point3f& pt);
void glColor(const pi::Point3ub& color);

void glMultMatrix(const pi::SE3f& pose);
#endif // GLHELPER_H
