/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#include "glHelper.h"

#include <GL/gl.h>

#define A360byPI 114.59155902616439

using namespace pi;
using namespace std;

void glVertex(const Point3d& pt) {glVertex3d(pt.x,pt.y,pt.z);}
void glVertex(const Point3f& pt) {glVertex3f(pt.x,pt.y,pt.z);}

void glColor(const pi::Point3ub& color){glColor3ub(color.x,color.y,color.z);}
void glMultMatrix(const pi::SE3f& pose)
{
    pi::Point3f trans=pose.get_translation();
    glTranslatef(trans.x,trans.y,trans.z);
    float x,y,z,w;
    pose.get_rotation().getValue(x,y,z,w);
    glRotatef(A360byPI*acos(w),x,y,z);
}

