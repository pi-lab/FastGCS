/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef OBJECT_3DS_H
#define OBJECT_3DS_H

#include "PosedObject.h"

class Lib3dsFile;
class Lib3dsNode;
namespace pi{
namespace gl{


class Object_3DS:public GL_Object
{
public:
    Object_3DS(const char* filename);

    void init();

    virtual void draw();

    bool isOpened(){return file;}

    void setPose(const pi::SE3f& ps){ScopedMutex lock(m_mutex);pose=ps;}
    const pi::SE3f& getPose(){ScopedMutex lock(m_mutex);return pose;}

private:
    void renderNode(Lib3dsNode *node);
    Lib3dsFile *file;
    pi::Mutex  m_mutex;
    pi::SE3f   pose;
};


}
}

#endif // OBJECT_3DS_H
