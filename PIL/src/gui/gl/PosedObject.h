/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef POSEDOBJECT_H
#define POSEDOBJECT_H

#include <base/types/SE3.h>
#include "GL_FatherObject.h"


namespace pi{
namespace gl{

class PosedObject:public GL_Object
{
public:
    PosedObject(const GL_ObjectPtr& object=GL_ObjectPtr()):obj(object){}

    bool setObject(const GL_ObjectPtr& object){ScopedMutex lock(m_mutex);obj=object;}

    bool setPose(const pi::SE3f& ps){ScopedMutex lock(m_mutex);pose=ps;}
    const pi::SE3f& getPose(){ScopedMutex lock(m_mutex);return pose;}

    virtual void draw();

private:
    GL_ObjectPtr    obj;
    pi::SE3f        pose;
    Mutex           m_mutex;
};

}}
#endif // POSEDOBJECT_H
