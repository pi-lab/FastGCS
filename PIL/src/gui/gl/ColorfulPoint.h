/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef COLORFULPOINT_H
#define COLORFULPOINT_H

#include "GL_Object.h"
#include <base/types/types.h>
#include <GL/gl.h>

namespace pi{
namespace gl{
typedef pi::Point3ub Color3b;

struct ColorfulPoint:public GL_Object
{
    ColorfulPoint(const pi::Point3f& Point=pi::Point3f(0.,0.,0.),
                  const Color3b&     Color=Color3b(255,255,255))
        :point(Point),color(Color){}

    virtual void draw()
    {
        glBegin(GL_POINT);
        glColor3b(color.x,color.y,color.z);
        glVertex3f(point.x,point.y,point.z);
        glEnd();
    }

    pi::Point3f  point;
    Color3b      color;
};

}}
#endif // COLORFULPOINT_H
