/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/


#ifndef MAINWINDOWIMPL_H
#define MAINWINDOWIMPL_H

#include <QMainWindow>
#include <queue>

class MainWindowImpl : public QMainWindow
{
    Q_OBJECT
public:
    MainWindowImpl(QWidget *parent = 0);

    virtual ~MainWindowImpl(){}

//    virtual int setupLayout(void);

    void    call(std::string cmd);

protected:
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void resizeEvent(QResizeEvent *event);
    void timerEvent(QTimerEvent *event);

signals:
    void call_signal();

protected slots:
    void call_slot();

public slots:
    void action_SvarEdit(void);

private:

    std::queue<std::string>       cmds;
};

#endif // MAINWINDOWIMPL_H
