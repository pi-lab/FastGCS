################################################################################
#
#  Pilot Intelligence Library
#    http://www.pilotintelligence.com/
#
#  ----------------------------------------------------------------------------
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
################################################################################


MOC_FILES    += Win3D \
                controls/qFlightInstruments controls/qcustomplot controls/SvarTable \
                controls/ImageViewer \
                gl/SignalHandle gl/Win3D MainWindowImpl 
MODULES      += PI_BASE QT QGLVIEWER EIGEN3 PI_LUA LUA OPENGL 3DS OPENCV

