
#ifndef __IMAGE_VIEWER_H__
#define __IMAGE_VIEWER_H__

#include <string>
#include <queue>

#include <QtGui>
#include <QWidget>

#include <opencv2/opencv.hpp>

namespace pi {

class ImageViewer : public QWidget
{
    Q_OBJECT

public:
    ImageViewer(QWidget *parent = 0);
    ~ImageViewer();

    int imshow(const std::string &name, cv::Mat &img);

    int call(const std::string& cmd);

protected:
    void keyPressEvent(QKeyEvent *e);

    void paintEvent(QPaintEvent *);
    void closeEvent(QCloseEvent *);

signals:
    void call_signal();

protected slots:
    void call_slot();

protected:
    QImage          *m_img;
    int             imgW, imgH;

    QMutex          *m_muxData;
    std::string     m_name;

    std::queue<std::string>     callCmds;
};

} // end of namespace pi

#endif // end of __IMAGE_VIEWER_H__


