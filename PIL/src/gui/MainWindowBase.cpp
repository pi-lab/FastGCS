/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#include <QApplication>

#include <base/Svar/Svar.h>

#include "MainWindowImpl.h"
#include "MainWindowBase.h"

using namespace std;

namespace pi {

MainWindowBase::MainWindowBase()
{
    start();
    while(!impl.get()) sleep(10);
}

void MainWindowBase::threadFunc()
{
    int argc=svar.GetInt("argc",1);

	// FIXME: need to fix
    SvarWithType<char**>& inst=SvarWithType<char**>::instance();
    char** argv;
	if( svar.exist("argv") ) argv = (char**) svar.GetPointer("argv", NULL);
	else					 argv = new char*[argc];
	
	QApplication app(argc,argv);
    impl=SPtr<MainWindowImpl>(new MainWindowImpl);

    int ret=app.exec();
    stop();
}

void MainWindowBase::call(string cmd)
{
    if(impl.get())
        impl->call(cmd);
}

QWidget* MainWindowBase::getWidget()
{
    if(impl.get())
        return impl.get();
}

} // end of namespace pi
