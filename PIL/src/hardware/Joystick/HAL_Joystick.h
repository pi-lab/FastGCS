/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef __HAL_JOYSTICK_H__
#define __HAL_JOYSTICK_H__

#include <stdint.h>
#include <map>

#include "base/osa/osa++.h"


namespace pi {

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define JS_AXIS_MAX_NUM     16
#define JS_BUTTON_MAX_NUM   32


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

struct JS_Val {
    float       AXIS[JS_AXIS_MAX_NUM];                  ///< axis values
    float       BUTTON[JS_BUTTON_MAX_NUM ];             ///< button state
    uint32_t    timeStamp;                              ///< state timestamp
    int         dataUpdated;                            ///< state has been readed or not
};


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class HAL_JoyStick: public Thread
{
public:
    HAL_JoyStick() {
        pi::Thread::setName("HAL_JoyStick");

        m_devID = -1;
        m_bOpened = 0;
    }

    HAL_JoyStick(int dev) {
        HAL_JoyStick();

        m_devID = dev;
    }

    virtual ~HAL_JoyStick() {
        close();

        m_channelMap.clear();
        m_btnMap.clear();
    }

    virtual void threadFunc();

    int open(int devID = 0);
    int close(void);

    int setChannelMap(std::vector<double> &channelMap);
    int setBtnMap(std::vector<double> &channelMap);

    int read(JS_Val *jsv);

    int isOpen(void) { return m_bOpened; }

public:
    int                 m_devType;                  // 0:joystick 1:control
    int                 m_devID;
    int                 m_devFD;

    char                m_devName[256];
    uint32_t            m_devVersion;
    uint8_t             m_numAxes;
    uint8_t             m_numBtns;

protected:
    Mutex               m_mutex;
    JS_Val              m_JSVal;
    int                 m_bOpened;

    std::map<int,int>   m_channelMap;
    std::map<int,int>   m_btnMap;
};

} // end of namespace pi

#endif // end of __HAL_JOYSTICK_H__
