/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef __AHRS_MINI__
#define __AHRS_MINI__

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>


#include "base/time/Timer.h"
#include "base/osa/osa++.h"
#include "hardware/UART/UART.h"

namespace pi {

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
struct AHRS_Frame
{
    enum FrameType {
        FRAME_AHRS,
        FRAME_SENSOR
    } frame_type;

    float       yaw, pitch, roll;           // degree
    float       alt, temp, press;           // meter, deg, pa
    int         imu_ps;                     // IMU per second

    int         Ax, Ay, Az;                 // accelation
    int         Gx, Gy, Gz;                 // gyro
    int         Mx, My, Mz;                 // megnatic

    int         crc,                        // CRC received
                crc_v,                      // CRC computed
                correct;                    // frame correct

    uint64_t    tm;                         // mills when receive

public:
    void print(void);
};


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class AHRS_Mini : public Thread
{
public:
    AHRS_Mini();
    AHRS_Mini(const std::string& port_name);

    virtual ~AHRS_Mini();

    int setUART(const std::string& port_name);

    virtual void start(void);
    virtual void stop(void);

    int frame_ready(void);
    AHRS_Frame frame_get(AHRS_Frame::FrameType ft = AHRS_Frame::FRAME_AHRS);

    int parse_frame(uint8_t *buf);

    virtual void threadFunc();

protected:
    AHRS_Frame  frame;
    AHRS_Frame  frame_imu, frame_sen;
    UART        uart;

    int         frame_aviable;
};

} // end of namespace pi

#endif // end of __AHRS_MINI__
