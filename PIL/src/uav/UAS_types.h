/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef __UAS_TYPES_H__
#define __UAS_TYPES_H__


#include <stdio.h>
#include <stdint.h>

#include <string>
#include <vector>
#include <map>
#include <deque>

#include <mavlink/v2.0/common/mavlink.h>
#include <mavlink/v2.0/ardupilotmega/ardupilotmega.h>

#include <base/utils/utils.h>
#include <base/osa/osa++.h>


////////////////////////////////////////////////////////////////////////////////
/// predefine class
////////////////////////////////////////////////////////////////////////////////
class UAS_Base;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/**
 * @brief The UAS Type enum
 *
 *  FIXME: parse MAVLINK message based on sysid
 *      0 ~  49: MAV
 *     50 ~ 249: Telemetry
 *    250 ~ 255: GCS
 */
enum UAS_Type
{
    UAS_TYPE_MAV     = 0,
    UAS_TYPE_GCS     = 1,
    UAS_TYPE_TELEM   = 2
};


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

struct AP_ParamItem
{
public:
    int             index;                      ///< index number
    char            id[17];                     ///< ID name
    MAV_PARAM_TYPE  type;                       ///< value type
    float           value;                      ///< value

    int             modified;                   ///< modified flag

public:
    int8_t      toInt8(void);
    uint8_t     toUint8(void);
    int16_t     toInt16(void);
    uint16_t    toUint16(void);
    int32_t     toInt32(void);
    uint32_t    toUint32(void);
    float       toFloat(void);

    void        fromInt8(int8_t v);
    void        fromUint8(uint8_t v);
    void        fromInt16(int16_t v);
    void        fromUint16(uint16_t v);
    void        fromInt32(int32_t v);
    void        fromUint32(uint32_t v);
    void        fromFloat(float v);

public:
    AP_ParamItem();
    virtual ~AP_ParamItem();

    void init();
    void release();
};

typedef std::map<int, AP_ParamItem*>            AP_ParamIndexMap;
typedef std::map<std::string, AP_ParamItem*>    AP_ParamIDMap;
typedef std::vector<AP_ParamItem*>              AP_ParamVector;


class AP_ParamArray
{
public:
    enum PARAM_RW_STATUS {
        IDLE,                           ///< idle state
        READING,                        ///< read single item
        READING_ALL,                    ///< read all items one by one
        READING_BATCH,                  ///< batch read all items (UAS auto send parameters)
        WRITING,                        ///< write single item
        WRITING_ALL                     ///< write all items
    };

public:
    AP_ParamArray();
    ~AP_ParamArray();

    void init(void);
    void release(void);

    int size(void);
    int reserve(int n);
    int clear(void);

    int lock(void);
    int unlock(void);

    int requireParameters(int clearOld = 0);
    int requireParameter(int index);
    int requireParameter(char *id);
    int _requireParameter(int index);
    int _requireParameter(char *id);

    int updateParameters(void);
    int updateParameters(pi::StringArray &lstParams);
    int updateParameter(int index);
    int updateParameter(char *id);
    int _updateParameter(AP_ParamItem *pi);

    int set(AP_ParamArray &pa);
    int set(AP_ParamItem &item);
    AP_ParamItem* get(int idx);
    AP_ParamItem* get(std::string id);

    AP_ParamVector* get_allParam(void);
    AP_ParamIndexMap* get_paramIndexMap(void);

    int set_paramN(int nParam);
    int get_paramN(void);
    int get_currIdx(void);

    int collectUnreadedItems(void);
    int getUnreadedItems(int n, std::vector<int> &lst);

    int isLoaded(void);
    int setLoaded(int bl);

    int setStatus(PARAM_RW_STATUS st);
    PARAM_RW_STATUS getStatus(int &nParamRW, int &idxCurr);
    PARAM_RW_STATUS getStatus(void);

    int get_tm_lastRead(uint64_t &t);

    int save(std::string fname);
    int load(std::string fname);

    virtual int timerFunction(void *arg);
    virtual int parseMavlinkMsg(mavlink_message_t *msg);

    void setUAS(UAS_Base *u) { m_uas = u; }

protected:
    virtual int startTimer(void);
    virtual int stopTimer(void);

protected:
    AP_ParamVector              m_lstAll;
    AP_ParamIndexMap            m_mapIndex;
    AP_ParamIDMap               m_mapID;
    AP_ParamVector              m_lstWrite;
    std::deque<int>             m_lstRead;

    int                         m_nParam;               ///< parameter total number
    int                         m_nParamRW;             ///< parameter number for r/w
    int                         m_idxReading;           ///< parameter index for reading
    int                         m_idxWritting;          ///< parameter index for writting
    int                         m_idxCurrent;           ///< current r/w index
    int                         m_bLoaded;              ///< full parameters loaded or not

    PARAM_RW_STATUS             m_stParamReading;       ///< parameter reading status
    uint64_t                    m_tLastReading;         ///< last read/write time

    pi::Mutex                   m_mutex;                ///< reading mutex
    pi::TimerTask               m_timer;                ///< timer

    UAS_Base                    *m_uas;                 ///< UAS
};


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


class AP_MissionItem
{
public:
    AP_MissionItem() { init(); }
    ~AP_MissionItem() { release(); }

    void init(void);
    void release(void);

    void set(double lat_, double lng_, double alt_);
    void setPos(double lat_, double lng_);
    void setAlt(double alt_);
    void setHeading(double heading_);

    void get(double &lat_, double &lng_, double &alt_);
    void getPos(double &lat_, double &lng_);
    void getAlt(double &alt_);
    void getHeading(double &heading_);

    int to_mission_item(mavlink_mission_item_t *mi);
    int from_mission_item(mavlink_mission_item_t *mi);

    void setWriteConfirm(int c) { writeConfirmed = c; }
    int  getWriteConfirm(void) { return writeConfirmed; }

    int compare(AP_MissionItem *wp);

public:
    int         idx;                    ///< index number

    MAV_CMD     cmd;                    ///< mission command

    double      lat, lng;               ///< latitdue & longtitude
    double      alt;                    ///< altitude above home
    double      heading;                ///< heading (0:North, 90: Est, 180: South, 270: West)

    float       param1;                 ///< PARAM1, see MAV_CMD enum
    float       param2;                 ///< PARAM2, see MAV_CMD enum
    float       param3;                 ///< PARAM3, see MAV_CMD enum
    float       param4;                 ///< PARAM4, see MAV_CMD enum

    int         current;                ///< current mission (1:True, 0:False)

    MAV_FRAME   frame;                  ///< The coordinate system of the MISSION. see MAV_FRAME in mavlink_types.h
    uint8_t     autocontinue;           ///< autocontinue to next wp

public:
    int         writeConfirmed;         ///< write confirmed
};


typedef std::map<int, AP_MissionItem*>     AP_MissionItemMap;
typedef std::vector<AP_MissionItem*>       AP_MissionItemVector;


class AP_MissionArray
{
public:
    enum WP_RW_STATUS {
        IDLE,                           ///< idle state

        READING_NUM,                    ///< request mission number
        READING_ITEM,                   ///< reading item

        WRITTING_NUM,                   ///< write mission number
        WRITTING_ITEM,                  ///< write item
        WRITTING_CONFIRM_NUM,           ///< confirm number
        WRITTING_CONFIRM_ITEM,          ///< confirm item

        CLEAR_WP                        ///< clear waypoints
    };

public:
    AP_MissionArray();
    ~AP_MissionArray();

    void init(void);
    void release(void);

    int set(AP_MissionArray *wpa);
    int set(AP_MissionItem &wp);
    int get(AP_MissionItem &wp);
    AP_MissionItem* get(int idx);
    int getAll(AP_MissionItemVector &wps);
    AP_MissionItemMap* getAll();

    int remove(int idx);
    int size(void);
    int clear(void);

    int save(std::string fname);
    int load(std::string fname);

    void setUAS(UAS_Base *u) { m_uas = u; }
    UAS_Base* getUAS(void) { return m_uas; }

    int writeWaypoints(void);
    int readWaypoints(void);
    int writeWaypointsNum(void);
    int readWaypointsNum(void);

    int clearWaypoints(void);
    int setCurrentWaypoint(int idx);
    int getCurrentWaypoint(void) { return m_currWaypoint; }

    WP_RW_STATUS getStatus(int &iRW, int &nRW) {
        iRW = m_iRW;
        nRW = m_nRW;
        return m_status;
    }

    virtual int timerFunction(void *arg);
    virtual int parseMavlinkMsg(mavlink_message_t *msg);

protected:
    virtual int startTimer(void);
    virtual int stopTimer(void);

    int _readWaypointsNum(void);
    int _readWaypoint(int idx);
    int _writeWaypoint(int idx);
    int _sendWritePartialList(int idx);


protected:
    AP_MissionItemMap   m_arrWP;        ///< waypoints map
    pi::TimerTask       m_timer;        ///< timer

    UAS_Base            *m_uas;         ///< UAS obj

    int                 m_iRW;          ///< current R/W waypoint
    int                 m_nRW;          ///< readed/written waypoints
    WP_RW_STATUS        m_status;       ///< status
    uint64_t            m_tLast;        ///< Last R/W timestamp (in ms)
    int                 m_currWaypoint; ///< current mission
    int                 m_nReadTry;     ///< Read try number

    std::deque<int>     m_wpList;       ///< waypoint write list
};

#endif // end of __UAS_TYPES_H__

