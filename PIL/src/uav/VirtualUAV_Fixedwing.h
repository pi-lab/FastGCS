/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef __VIRTUALUAV_FIXEDWING_H__
#define __VIRTUALUAV_FIXEDWING_H__

#include "JSBSim.h"
#include "VirtualUAV.h"


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class VirtualUAV_Fixedwing : public VirtualUAV
{
public:
    VirtualUAV_Fixedwing();
    virtual ~VirtualUAV_Fixedwing();

    int init();
    int release();

    virtual int timerFunction(void *arg);
    virtual int simulation(pi::JS_Val *jsv);
    virtual int toFlightGear(FGNetFDM *fgData);

    virtual int autoNavigate(float *js);
    virtual int autoNavigate_takeoff(float *js);
    virtual int autoNavigate_mission(float *js);

    virtual int executeCommand(const std::string& cmd);

protected:
    std::string     m_modelName;

    int             m_numMotor;

    int             m_startPOS_set;
    double          m_startPOS_lat;
    double          m_startPOS_lng;
    double          m_startPOS_alt;

    int             m_engineStart;
    int             m_engineStartTM;

    int             m_takeoffState;

    pi::JSBSim      m_jsbSim;

    double          m_tmLastAltSet;
};


#endif // end of  __VIRTUALUAV_FIXEDWING_H__

