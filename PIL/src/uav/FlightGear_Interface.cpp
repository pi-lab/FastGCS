/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#include "base/Svar/Svar.h"
#include "network/Socket++.h"

#include "FlightGear_Interface.h"


using namespace std;
using namespace pi;


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

FlightGear_Transfer::FlightGear_Transfer()
{
    //SvarWithType<FlightGear_Transfer*>::instance()["FlightGear_Transfer.ptr"] = this;
    svar.GetPointer("FlightGear_Transfer.ptr", NULL) = this;

    m_pData = new RSocket();
}

FlightGear_Transfer::~FlightGear_Transfer()
{
    RSocket *s = (RSocket *) m_pData;

    if( s != NULL ) {
        delete s;
        m_pData = NULL;
    }

    svar.GetPointer("FlightGear_Transfer.ptr", NULL) = NULL;
}

int FlightGear_Transfer::connect(const std::string &hn, int port)
{
    RSocket *s = (RSocket *) m_pData;

    if( s == NULL ) return -1;

    return s->startClient(hn, port, SOCKET_UDP);
}

int FlightGear_Transfer::close(void)
{
    RSocket *s = (RSocket *) m_pData;

    if( s == NULL ) return -1;

    return s->close();
}

int FlightGear_Transfer::isRunning(void)
{
    RSocket *s = (RSocket *) m_pData;

    if( s == NULL ) return 0;
    else return 1;
}

int FlightGear_Transfer::trans(FGNetFDM *fdm)
{
    RSocket *s = (RSocket *) m_pData;

    if( s == NULL ) return -1;

    return s->send((uint8_t*) fdm, sizeof(*fdm));
}
