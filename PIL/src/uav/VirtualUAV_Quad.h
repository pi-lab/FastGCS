/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef __VIRTUALUAV_QUAD_H__
#define __VIRTUALUAV_QUAD_H__


#include "VirtualUAV.h"


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class VirtualUAV_Quad : public VirtualUAV
{
public:
    VirtualUAV_Quad();
    virtual ~VirtualUAV_Quad();

    int init();
    int release();

    virtual int timerFunction(void *arg);
    virtual int simulation(pi::JS_Val *jsv);
    virtual int toFlightGear(FGNetFDM *fgData);

    virtual int autoNavigate(float *js);

protected:
    double          sim_dragK;

    int             m_numMotor;
};


#endif // end of __VIRTUALUAV_QUAD_H__
