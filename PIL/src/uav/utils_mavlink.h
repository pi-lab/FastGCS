/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/

#ifndef __MAVLINK_UTILS_H__
#define __MAVLINK_UTILS_H__

#include <stdint.h>

#include <string>
#include <list>
#include <vector>

#include <mavlink/v2.0/common/mavlink.h>
#include <mavlink/v2.0/ardupilotmega/ardupilotmega.h>

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

typedef std::vector<std::string> mavlink_nameList;

////////////////////////////////////////////////////////////////////////////////
///  enum of flight mode
////////////////////////////////////////////////////////////////////////////////

enum Mavlink_px4_FlightMode
{
    MFM_MANUAL,
    MFM_ALTCTL,
    MFM_POSCTL,
    MFM_AUTO_MISSION,
    MFM_AUTO_LOITER,
    MFM_AUTO_RTL,
    MFM_AUTO_LANGENGFAIL,             //same as AUTO_LAND
    MFM_ACRO,
    MFM_DESCEND,                      //same as AUTO_LAND
    MFM_TERMINATION,                  //same as MANUAL mode
    MFM_OFFBOARD,
    MFM_STABILIZED,
    MFM_AUTO_TAKEOFF,
    MFM_AUTO_LAND,
    MFM_AUTO_FOLLOW_TARGET,
    MFM_AUTO_PRECLAND,
    MFM_ORBIT
};

#if 0
//below are for APM flightMode reference
MFM_STABILIZE,                  //  0, manual airframe angle with manual throttle
MFM_ACRO,                       //  1, manual body-frame angular rate with manual throttle
MFM_ALT_HOLD,                   //  2, manual airframe angle with automatic throttle
MFM_AUTO,                       //  3, fully automatic waypoint control using mission commands
MFM_GUIDED,                     //  4, fully automatic fly to coordinate or fly at velocity/direction using GCS immediate commands
MFM_LOITER,                     //  5, automatic horizontal acceleration with automatic throttle
MFM_RTL,                        //  6, automatic return to launching point
MFM_CIRCLE,                     //  7, automatic circular flight with automatic throttle
MFM_NONE1,                      //  8, NONE
MFM_LAND,                       //  9, automatic landing with horizontal position control
MFM_OF_LOITER,                  // 10, deprecated
MFM_DRIFT,                      // 11, semi-automous position, yaw and throttle control
MFM_NONE2,                      // 12, NONE
MFM_SPORT,                      // 13, manual earth-frame angular rate control with manual throttle
MFM_FLIP,                       // 14, automatically flip the vehicle on the roll axis
MFM_AUTOTUNE,                   // 15, automatically tune the vehicle's roll and pitch gains
MFM_POSHOLD,                    // 16, automatic position hold with manual override, with automatic throttle
MFM_BRAKE,                      // 17, full-brake using inertial/GPS system, no pilot input
#endif

////////////////////////////////////////////////////////////////////////////////
/// enum to name
////////////////////////////////////////////////////////////////////////////////
int mavlink_autopilot_name(uint8_t ap, char **name);
int mavlink_mav_type_name(uint8_t mt, char **name);
int mavlink_mav_basemode_name(uint8_t mf, mavlink_nameList &nl);
int mavlink_px4_custommode_getName(uint32_t cm, char **name, uint8_t& cmEnum);
uint32_t mavlink_px4_custommode_getInt(const char *name);
int mavlink_px4_custommode_getID(const char* name);
int mavlink_mav_state_name(uint8_t ms, char **name);

void        mavlink_mav_cmdList(mavlink_nameList &nl);
std::string mavlink_mav_cmd2str(MAV_CMD cmd);
MAV_CMD     mavlink_mav_str2cmd(const std::string &cmd);


////////////////////////////////////////////////////////////////////////////////
/// MAV_SYS_STATUS_SENSOR
////////////////////////////////////////////////////////////////////////////////
#define MAV_SYS_STATUS_SENSOR_ID_NAME_DEF(id, name, desc) { id, name, desc }

struct MAVLINK_SYS_STATUS_SENSOR_IDNAME_Struct {
    uint32_t    id;
    char        name[256];
    char        desc[256];
};

typedef  std::vector<uint32_t>     mavlink_sys_status_sensor_list;
typedef  std::vector<std::string>  mavlink_sys_status_sensor_namelist;

int mavlink_sys_status_sensor_getIDs(uint32_t s, mavlink_sys_status_sensor_list &l);
int mavlink_sys_status_sensor_getName(uint32_t id, char **name);
int mavlink_sys_status_sensor_getNames(mavlink_sys_status_sensor_list &l,
                                        mavlink_sys_status_sensor_namelist &nl);

int mavlink_sys_status_sensor_getID_Difference(mavlink_sys_status_sensor_list &l1,
                                               mavlink_sys_status_sensor_list &l2,
                                               mavlink_sys_status_sensor_list &ld);


////////////////////////////////////////////////////////////////////////////////
/// Data stream ID functions
////////////////////////////////////////////////////////////////////////////////

std::vector<int> mavlink_getStreamIDs(void);



////////////////////////////////////////////////////////////////////////////////
/// utils
////////////////////////////////////////////////////////////////////////////////


/**
 *  Value averager
 */
template<class T>
class ValueAverager
{
public:
    ValueAverager() {
        init();

        valArray.resize(nMax);
    }

    ValueAverager(int n) {
        init();

        nMax = n;
        valArray.resize(nMax);
    }

    ~ValueAverager() {
        init();
    }

    T push(T v) {
        valArray[idx%nMax] = v;
        num ++;
        idx ++;

        return calcAvg();
    }

    T calcAvg(void) {
        T   avg;
        int i;

        avg = 0;
        if( idx < nMax ) {
            for(i=0; i<idx; i++) avg += valArray[i];

            avg = avg / idx;
        } else {
            for(i=0; i<nMax; i++) avg += valArray[i];

            avg = avg / nMax;
        }

        return avg;
    }

    void setSize(int n) {
        init();

        nMax = n;
        valArray.resize(nMax);
    }

    void init(void) {
        nMax = 10;
        num  = 0;
        idx  = 0;
        valArray.clear();
    }

public:
    int                 nMax, num, idx;
    std::vector<T>      valArray;
};



#endif // end of __MAVLINK_UTILS_H__
