/*******************************************************************************

  Pilot Intelligence Library
    http://www.pilotintelligence.com/

  ----------------------------------------------------------------------------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************/


#include <algorithm>

#include "base/osa/osa++.h"
#include "base/utils/utils.h"

#include "UAS_types.h"
#include "UAS.h"

using namespace pi;



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

AP_ParamItem::AP_ParamItem()
{
    init();
}

AP_ParamItem::~AP_ParamItem()
{
    release();
}

void AP_ParamItem::init()
{
    index = 0;
    memset(id, 0, 17);
    type = MAV_PARAM_TYPE_REAL32;
    value = 0;

    modified = 0;
}

void AP_ParamItem::release()
{
    index = 0;
    memset(id, 0, 17);
    type = MAV_PARAM_TYPE_REAL32;
    value = 0;

    modified = 0;
}

int8_t      AP_ParamItem::toInt8(void)
{
    return (int8_t) value;
}

uint8_t     AP_ParamItem::toUint8(void)
{
    return (uint8_t) value;
}

int16_t     AP_ParamItem::toInt16(void)
{
    return (int16_t) value;
}

uint16_t    AP_ParamItem::toUint16(void)
{
    return (uint16_t) value;
}

int32_t     AP_ParamItem::toInt32(void)
{
    return (int32_t) value;
}

uint32_t    AP_ParamItem::toUint32(void)
{
    return (uint32_t) value;
}

float       AP_ParamItem::toFloat(void)
{
    return value;
}

void        AP_ParamItem::fromInt8(int8_t v)
{
    value = v;
}

void        AP_ParamItem::fromUint8(uint8_t v)
{
    value = v;
}

void        AP_ParamItem::fromInt16(int16_t v)
{
    value = v;
}

void        AP_ParamItem::fromUint16(uint16_t v)
{
    value = v;
}

void        AP_ParamItem::fromInt32(int32_t v)
{
    value = v;
}

void        AP_ParamItem::fromUint32(uint32_t v)
{
    value = v;
}

void        AP_ParamItem::fromFloat(float v)
{
    value = v;
}



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void AP_ParamArray_timerFunc(void *arg)
{
    AP_ParamArray *pa = (AP_ParamArray*) arg;

    pa->timerFunction(arg);
}

AP_ParamArray::AP_ParamArray()
{
    m_timer.setName("AP_ParamArray.Timer");

    m_uas = NULL;

    init();
}

AP_ParamArray::~AP_ParamArray()
{
    release();

    // stop timer
    stopTimer();

    m_uas = NULL;
}

void AP_ParamArray::init(void)
{
    m_nParam = 0;
    m_nParamRW = 0;
    m_idxCurrent = 0;
    m_idxReading = 0;
    m_idxWritting = 0;

    m_lstAll.clear();
    m_lstWrite.clear();
    m_mapIndex.clear();
    m_mapID.clear();

    m_stParamReading = IDLE;
    m_tLastReading = 0;

    m_bLoaded = 0;
}

void AP_ParamArray::release(void)
{
    std::vector<AP_ParamItem*>::iterator it;

    for(it=m_lstAll.begin(); it!=m_lstAll.end(); it++) {
        delete *it;
    }
    m_lstAll.clear();
    m_lstWrite.clear();
    m_lstRead.clear();

    m_mapIndex.clear();
    m_mapID.clear();

    m_bLoaded = 0;
}

int AP_ParamArray::clear(void)
{
    m_mutex.lock();

    release();

    m_nParam = 0;
    m_nParamRW = 0;
    m_idxCurrent = 0;
    m_idxReading = 0;
    m_idxWritting = 0;

    m_stParamReading = IDLE;
    m_tLastReading = 0;

    m_mutex.unlock();

    return 0;
}

int AP_ParamArray::lock(void)
{
    m_mutex.lock();

    return 0;
}

int AP_ParamArray::unlock(void)
{
    m_mutex.unlock();

    return 0;
}


int AP_ParamArray::requireParameters(int clearOld)
{
    mavlink_param_request_list_t packet;
    mavlink_message_t msg;

    if( m_stParamReading != IDLE ) return -1;

    // clear old contents
    if( clearOld ) clear();

    // send request msg
    if( 1 ) {
        packet.target_system    = m_uas->ID;
        packet.target_component = m_uas->compID;
        mavlink_msg_param_request_list_encode(m_uas->gcsID, m_uas->gcsCompID,
                                              &msg, &packet);

        m_uas->sendMavlinkMsg(&msg);
        setStatus(READING_BATCH);
    } else {
        _requireParameter(0);
        setStatus(READING_ALL);
    }

    m_nParam            = 1;                   // FIXME: set 1 by default
    m_nParamRW          = 1;
    m_idxCurrent        = 0;
    m_idxReading        = 0;
    m_tLastReading      = tm_get_millis();
    m_bLoaded           = 0;

    return 0;
}

int AP_ParamArray::_requireParameter(int index)
{
    mavlink_param_request_read_t packet;
    mavlink_message_t msg;

    packet.target_system    = m_uas->ID;
    packet.target_component = m_uas->compID;
    packet.param_index      = index;
    memset(packet.param_id, 0, 16);

    mavlink_msg_param_request_read_encode(m_uas->gcsID, m_uas->gcsCompID,
                                          &msg, &packet);

    m_uas->sendMavlinkMsg(&msg);

    m_tLastReading = tm_get_millis();

    return 0;
}

int AP_ParamArray::_requireParameter(char *id)
{
    mavlink_param_request_read_t packet;
    mavlink_message_t msg;


    packet.target_system    = m_uas->ID;
    packet.target_component = m_uas->compID;
    packet.param_index      = -1;
    strncpy(packet.param_id, id, 16);

    mavlink_msg_param_request_read_encode(m_uas->gcsID, m_uas->gcsCompID,
                                          &msg, &packet);

    m_uas->sendMavlinkMsg(&msg);

    m_tLastReading = tm_get_millis();

    return 0;
}

int AP_ParamArray::requireParameter(int index)
{
    if( m_stParamReading != IDLE ) return -1;

    _requireParameter(index);

    setStatus(READING);
    m_nParamRW = 1;
    m_idxReading = index;
    m_idxCurrent = 0;

    return 0;
}

int AP_ParamArray::requireParameter(char *id)
{
    if( m_stParamReading != IDLE ) return -1;

    _requireParameter(id);
    AP_ParamItem *p = get(id);

    setStatus(READING);
    m_nParamRW = 1;
    m_idxReading = p->index;
    m_idxCurrent = 0;

    return 0;
}

int AP_ParamArray::updateParameters(void)
{
    AP_ParamVector::iterator    it;
    AP_ParamItem                *p;

    // check current state
    if( m_stParamReading != IDLE ) return -1;

    // clear write list
    m_lstWrite.clear();

    // get all modified items
    for(it=m_lstAll.begin(); it!=m_lstAll.end(); it++) {
        p = *it;

        if( p->modified ) m_lstWrite.push_back(p);
    }

    // check modified item number
    if( m_lstWrite.size() < 1 ) return 0;

    setStatus(WRITING_ALL);
    m_nParamRW = m_lstWrite.size();
    m_idxCurrent = 0;

    // update first item
    p = m_lstWrite[0];
    m_idxWritting = p->index;
    _updateParameter(p);

    return 0;
}

int AP_ParamArray::updateParameters(StringArray &lstParams)
{
    StringArray::iterator    it;
    AP_ParamItem             *p;

    // check current state
    if( m_stParamReading != IDLE ) return -1;

    // clear write list
    m_lstWrite.clear();

    // get all modified items
    for(it=lstParams.begin(); it!=lstParams.end(); it++) {
        p = get(*it);
        if( p != NULL ) m_lstWrite.push_back(p);
    }

    // check modified item number
    if( m_lstWrite.size() < 1 ) return 0;

    setStatus(WRITING_ALL);
    m_nParamRW = m_lstWrite.size();
    m_idxCurrent = 0;

    // update first item
    p = m_lstWrite[0];
    m_idxWritting = p->index;
    _updateParameter(p);

    return 0;
}

int AP_ParamArray::_updateParameter(AP_ParamItem *pi)
{
    mavlink_param_set_t packet;
    mavlink_message_t msg;

    packet.target_system    = m_uas->ID;
    packet.target_component = m_uas->compID;
    packet.param_type       = (uint8_t) pi->type;
    packet.param_value      = pi->value;
    strncpy(packet.param_id, pi->id, 16);

    mavlink_msg_param_set_encode(m_uas->gcsID, m_uas->gcsCompID,
                                 &msg, &packet);
    m_uas->sendMavlinkMsg(&msg);

    m_idxWritting = pi->index;
    m_tLastReading = tm_get_millis();

    return 0;
}

int AP_ParamArray::updateParameter(int index)
{
    AP_ParamItem *pi;

    if( m_stParamReading != IDLE ) return -1;

    pi = get(index);
    _updateParameter(pi);

    m_nParamRW = 1;
    m_idxCurrent = 0;
    setStatus(WRITING);

    return 0;
}

int AP_ParamArray::updateParameter(char *id)
{
    AP_ParamItem *pi;

    if( m_stParamReading != IDLE ) return -1;

    pi = get(id);
    _updateParameter(pi);

    m_nParamRW = 1;
    m_idxCurrent = 0;
    setStatus(WRITING);

    return 0;
}



int AP_ParamArray::set(AP_ParamArray &pa)
{
    std::vector<AP_ParamItem*>::iterator it;
    AP_ParamItem *pi, *pi2;

    // copy all items
    for(it=pa.m_lstAll.begin(); it!=pa.m_lstAll.end(); it++) {
        pi = *it;

        pi2 = get(pi->id);
        if( pi2 == NULL ) {
            set(*pi);
        } else {
            pi2->type = pi->type;
            if( pi2->value != pi->value ) {
                pi2->modified = 1;
            }

            pi2->value = pi->value;
        }
    }

    // set flags
    m_mutex.lock();

    m_stParamReading = IDLE;
    m_tLastReading = tm_get_millis();
    m_bLoaded = 0;

    m_mutex.unlock();

    return 0;
}

int AP_ParamArray::set(AP_ParamItem &item)
{
    AP_ParamItem    *pi;

    pi = get(item.id);

    m_mutex.lock();

    m_tLastReading = tm_get_millis();
    m_idxCurrent = item.index;

    if( pi != NULL ) {
        pi->value = item.value;
        pi->modified = item.modified;
    } else {
        AP_ParamItem *ni;

        ni = new AP_ParamItem;
        *ni = item;

        m_lstAll.push_back(ni);
        m_mapIndex[item.index] = ni;
        m_mapID[item.id] = ni;
    }

    m_mutex.unlock();

    return 0;
}

AP_ParamItem* AP_ParamArray::get(int idx)
{
    AP_ParamItem *pi = NULL;
    std::map<int, AP_ParamItem*>::iterator it;

    m_mutex.lock();

    it = m_mapIndex.find(idx);
    if( it != m_mapIndex.end() ) {
        pi = it->second;
    }

    m_mutex.unlock();

    return pi;
}

AP_ParamItem* AP_ParamArray::get(std::string id)
{
    AP_ParamItem *pi = NULL;
    std::map<std::string, AP_ParamItem*>::iterator it;

    m_mutex.lock();

    it = m_mapID.find(id);
    if( it != m_mapID.end() ) {
        pi = it->second;
    }

    m_mutex.unlock();

    return pi;
}

AP_ParamVector* AP_ParamArray::get_allParam(void)
{
    return &m_lstAll;
}

AP_ParamIndexMap* AP_ParamArray::get_paramIndexMap(void)
{
    return &m_mapIndex;
}


int AP_ParamArray::set_paramN(int nParam)
{
    m_mutex.lock();
    m_nParam = nParam;
    m_mutex.unlock();

    return 0;
}

int AP_ParamArray::get_paramN(void)
{
    int n;

    m_mutex.lock();
    n = m_nParam;
    m_mutex.unlock();

    return n;
}

int AP_ParamArray::get_currIdx(void)
{
    int idx;

    m_mutex.lock();
    idx = m_idxCurrent;
    m_mutex.unlock();

    return idx;
}

int AP_ParamArray::collectUnreadedItems(void)
{
    // get unreaded items
    m_lstRead.clear();

    for(int i=0; i<m_nParamRW; i++) {
        if( m_mapIndex.find(i) == m_mapIndex.end() ) {
            m_lstRead.push_back(i);
        }
    }

    return 0;
}

int AP_ParamArray::getUnreadedItems(int n, std::vector<int> &lst)
{
    int i = 0, idx;

    // reserve n item
    lst.clear();
    lst.reserve(n);

    // insert to list
    while(m_lstRead.size() > 0 ) {
        if( i >= n ) break;

        idx = m_lstRead.front();
        lst.push_back(idx);
        m_lstRead.pop_front();

        i++;
    }

    return 0;
}

int AP_ParamArray::isLoaded(void)
{
    int l;

    m_mutex.lock();
    l = m_bLoaded;
    m_mutex.unlock();

    return l;
}

int AP_ParamArray::setLoaded(int bl)
{
    m_mutex.lock();
    m_bLoaded = bl;
    m_mutex.unlock();

    return 0;
}

int AP_ParamArray::setStatus(PARAM_RW_STATUS st)
{
    m_mutex.lock();
    m_stParamReading = st;
    m_mutex.unlock();

    return 0;
}

AP_ParamArray::PARAM_RW_STATUS AP_ParamArray::getStatus(int &nParamRW, int &idxCurr)
{
    AP_ParamArray::PARAM_RW_STATUS st;

    m_mutex.lock();

    nParamRW = m_nParamRW;
    idxCurr  = m_idxCurrent;
    st       = m_stParamReading;

    m_mutex.unlock();

    return st;
}

AP_ParamArray::PARAM_RW_STATUS AP_ParamArray::getStatus()
{
    AP_ParamArray::PARAM_RW_STATUS st;

    m_mutex.lock();
    st = m_stParamReading;
    m_mutex.unlock();

    return st;
}

int AP_ParamArray::get_tm_lastRead(uint64_t &t)
{
    m_mutex.lock();
    t = m_tLastReading;
    m_mutex.unlock();

    return 0;
}

int AP_ParamArray::save(std::string fname)
{
    FILE                        *fp = NULL;
    AP_ParamItem                *p;

    // open file
    fp = fopen(fname.c_str(), "wt");
    if( fp == NULL ) {
        dbg_pe("Cannot open file: %s", fname.c_str());
        return -1;
    }

    m_mutex.lock();

    int nParam = m_mapIndex.size();


    // output waypoints
    fprintf(fp, "#AP parameter number\n");
    fprintf(fp, "%d\n", nParam);

    fprintf(fp, "#parameter list\n");
    fprintf(fp, "# index    ID/name    type   value\n");

    for(int i=0; i<nParam; i++) {
        p = m_mapIndex[i];

        fprintf(fp, "%4d %17s %2d %f\n",
                p->index,
                p->id,
                (int)(p->type),
                p->value);
    }

    m_mutex.unlock();

    // close file
    fclose(fp);

    return 0;
}

int AP_ParamArray::load(std::string fname)
{
    FILE                        *fp = NULL;

    char                        *buf;
    std::string                 _b;
    int                         max_line_size;
    int                         s, idx, n;
    int                         i1, i2;
    float                       val;

    // open file
    fp = fopen(fname.c_str(), "rt");
    if( fp == NULL ) {
        dbg_pe("Cannot open file: %s", fname.c_str());
        return -1;
    }

    // free old contents
    clear();

    // alloc memory buffer
    max_line_size = 1024;
    buf = new char[max_line_size];

    s = 0;
    idx = 0;

    while(!feof(fp)) {
        // read a line
        if( NULL == fgets(buf, max_line_size, fp) )
            break;

        // remove blank & CR
        _b = trim(buf);

        if( _b.size() < 1 )
            continue;

        // skip comment
        if( _b[0] == '#' || _b[0] == ':' )
            continue;

        if( s == 0 ) {
            // read wp number
            sscanf(_b.c_str(), "%d", &n);
            m_nParam = n;
            s = 1;
        } else {
            AP_ParamItem pi;

            // read wp item
            sscanf(_b.c_str(), "%d %s %d %f",
                   &i1, pi.id,
                   &i2, &val);

            pi.index = i1;
            pi.type = (MAV_PARAM_TYPE) i2;
            pi.value = val;
            pi.modified = 0;

            // insert to map
            set(pi);

            idx ++;
        }
    }

    // set flags
    m_mutex.lock();

    m_stParamReading = IDLE;
    m_tLastReading = tm_get_millis();
    m_bLoaded = 0;

    m_mutex.unlock();

    // free buffer
    delete [] buf;

    // close file
    fclose(fp);

    return 0;
}



int AP_ParamArray::parseMavlinkMsg(mavlink_message_t *msg)
{
    if ( msg->msgid == MAVLINK_MSG_ID_PARAM_VALUE ) {
        mavlink_param_value_t pv;
        mavlink_msg_param_value_decode(msg, &pv);

        AP_ParamItem pi, *p;

        for(int i=0; i<16; i++) pi.id[i] = pv.param_id[i];
        pi.id[16] = 0;
        pi.index = pv.param_index;
        pi.type  = (MAV_PARAM_TYPE) pv.param_type;
        pi.value = pv.param_value;

        dbg_pt("[MAV:%3d %4d/%4d] ID: %16s, TYPE: %2d, VALUE: %f\n",
               m_uas->ID,
               pi.index, pv.param_count,
               pi.id, pv.param_type, pv.param_value);

        m_tLastReading = tm_get_millis();

        // processing received value
        if( m_stParamReading == WRITING ) {
            p = get(m_idxWritting);

            // confirm wrote value
            if( fabs(p->value-pi.value) < 1e6 && p->type == pi.type ) {
                dbg_pt("[MAV:%3d %4d/%4d] ID: %16s, TYPE: %2d, VALUE: %f (confirmed)\n",
                       m_uas->ID,
                       pi.index, pv.param_count,
                       pi.id, pv.param_type, pv.param_value);

                p->modified = 0;
                m_stParamReading = IDLE;
            } else {
                _updateParameter(p);
            }
        } else if ( m_stParamReading == WRITING_ALL ) {
            p = get(m_idxWritting);

            // confirm wrote value
            if( fabs(p->value-pi.value) < 1e6 && p->type == pi.type ) {
                dbg_pt("[MAV:%3d %4d/%4d] ID: %16s, TYPE: %2d, VALUE: %f (confirmed)\n",
                       m_uas->ID,
                       pi.index, pv.param_count,
                       pi.id, pv.param_type, pv.param_value);

                p->modified = 0;

                // update next parameter or stop
                if( m_idxCurrent < m_nParamRW-1 ) {
                    m_idxCurrent++;
                    p = m_lstWrite[m_idxCurrent];
                    _updateParameter(p);
                } else {
                    m_stParamReading = IDLE;
                }
            } else {
                _updateParameter(p);
            }
        } else if ( m_stParamReading == READING ) {
            set_paramN(pv.param_count);
            set(pi);
            m_stParamReading = IDLE;
            m_bLoaded = 1;
        } else if ( m_stParamReading == READING_BATCH ) {
            set_paramN(pv.param_count);
            m_nParamRW = m_nParam;
            set(pi);

            if( m_lstAll.size() == m_nParam ) {
                m_stParamReading = IDLE;
                m_bLoaded = 1;
            } else {
                m_idxCurrent = m_lstAll.size() - 1;
            }
        } else if ( m_stParamReading == READING_ALL ) {
            set_paramN(pv.param_count);
            m_nParamRW = m_nParam;
            set(pi);

            m_idxCurrent = m_lstAll.size() - 1;

            // check if all readed
            if( m_lstRead.size() < 1 ) {
                collectUnreadedItems();

                if( m_lstRead.size() < 1 ) {
                    m_stParamReading = IDLE;
                    m_bLoaded = 1;

                    return 1;
                }
            }

            // send request
            std::vector<int> l;
            getUnreadedItems(5, l);
            for(int i=0; i<l.size(); i++) {
                m_idxReading = l[i];
                _requireParameter(m_idxReading);
            }
        }

        return 1;
    }

    return 0;
}

int AP_ParamArray::timerFunction(void *arg)
{
    uint64_t tnow = tm_get_millis(), dt;

    if( tnow <= m_tLastReading ) return 0;
    dt = tnow - m_tLastReading;

    switch( m_stParamReading ) {
    case IDLE:
        //stopTimer();

        break;

    case READING:
        if( dt > 500 ) {
            _requireParameter(m_idxReading);
        }

        break;

    case READING_ALL:
        if( dt > 500 ) {
            // send request
            std::vector<int> l;
            getUnreadedItems(5, l);

            for(int i=0; i<l.size(); i++) {
                m_idxReading = l[i];
                _requireParameter(m_idxReading);
            }
        }

        break;

    case READING_BATCH:
        if( dt > 1000 ) {
            // if no one item readed
            if( m_idxCurrent == 0 && m_nParamRW == 1 ) {
                m_stParamReading = IDLE;
                requireParameters();

                break;
            }

            // get unreaded items
            collectUnreadedItems();
            m_stParamReading = READING_ALL;

            // send request
            std::vector<int> l;
            getUnreadedItems(5, l);

            for(int i=0; i<l.size(); i++) {
                m_idxReading = l[i];
                _requireParameter(m_idxReading);
            }
        }

        break;

    case WRITING:
        if( dt > 1000 ) {
            AP_ParamItem *p = get(m_idxWritting);
            _updateParameter(p);
        }

        break;

    case WRITING_ALL:
        if( dt > 1000 ) {
            AP_ParamItem *p = get(m_idxWritting);
            _updateParameter(p);
        }

        break;
    }

    return 0;
}

int AP_ParamArray::startTimer(void)
{
    if( !m_timer.isRunning() ) {
        m_timer.setPeriodicInterval(100);
        m_timer.start(AP_ParamArray_timerFunc, this);
    }

    return 0;
}

int AP_ParamArray::stopTimer(void)
{
    if( m_timer.isRunning() ) {
        m_timer.stop();
    }

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void AP_MissionItem::init(void)
{
    idx = -1;

    cmd = MAV_CMD_NAV_WAYPOINT;

    lat = -9999;
    lng = -9999;
    alt = 50;

    heading = 0;

    param1 = 0;
    param2 = 0;
    param3 = 0;
    param4 = 0;

    current = 0;

    frame = MAV_FRAME_GLOBAL;
    autocontinue = 1;

    writeConfirmed = 0;
}

void AP_MissionItem::release(void)
{
    writeConfirmed = 0;

    return;
}

void AP_MissionItem::set(double lat_, double lng_, double alt_)
{
    lat = lat_;
    lng = lng_;
    alt = alt_;
}

void AP_MissionItem::setPos(double lat_, double lng_)
{
    lat = lat_;
    lng = lng_;
}

void AP_MissionItem::setAlt(double alt_)
{
    alt = alt_;
}

void AP_MissionItem::setHeading(double heading_)
{
    heading = heading_;
}

void AP_MissionItem::get(double &lat_, double &lng_, double &alt_)
{
    lat_ = lat;
    lng_ = lng;
    alt_ = alt;
}

void AP_MissionItem::getPos(double &lat_, double &lng_)
{
    lat_ = lat;
    lng_ = lng;
}

void AP_MissionItem::getAlt(double &alt_)
{
    alt_ = alt;
}

void AP_MissionItem::getHeading(double &heading_)
{
    heading_ = heading;
}


int AP_MissionItem::to_mission_item(mavlink_mission_item_t *mi)
{
    mi->command         = cmd;           // mission type
    mi->seq             = idx;           // mission index

    mi->param1          = param1;
    mi->param2          = param2;
    mi->param3          = param3;
    mi->param4          = param4;

    mi->x               = lat;
    mi->y               = lng;
    mi->z               = alt;

    mi->frame           = frame;
    mi->autocontinue    = autocontinue;

    if( idx == 0 )
        mi->current     = 1;
    else
        mi->current     = 0;

    if( cmd == MAV_CMD_NAV_WAYPOINT ) {
        // stay time (in second)
        mi->param1 = svar.GetDouble("Mavlink.missionItem.Waypoint.StayTime", 1.2);
    } else if( cmd == MAV_CMD_NAV_TAKEOFF ) {
        // minimum pitch (plane only)
        mi->param1 = svar.GetDouble("Mavlink.missionItem.Takeoff.MinPitch", 5);
    } else if( cmd == MAV_CMD_NAV_LAND ) {
        // abort target altitude(m)  (plane only)
        mi->param1 = svar.GetDouble("Mavlink.missionItem.Land.TargetAlt", 0);
    }

    return 0;
}

int AP_MissionItem::from_mission_item(mavlink_mission_item_t *mi)
{
    cmd             = (MAV_CMD) mi->command;    // mission type
    idx             = mi->seq;                  // mission index

    param1          = mi->param1;
    param2          = mi->param2;
    param3          = mi->param3;
    param4          = mi->param4;

    lat             = mi->x;
    lng             = mi->y;
    alt             = mi->z;

    frame           = (MAV_FRAME) mi->frame;
    autocontinue    = mi->autocontinue;

    current         = mi->current;

    return 0;
}


int AP_MissionItem::compare(AP_MissionItem *wp)
{
    double          diff, diff_thr = 0.00005;
    AP_MissionItem     *p1 = this, *p2 = wp;
    int             fail = 0;

    // FIXME: better way?
    diff = fabs(p1->cmd - p2->cmd); //printf("diff_cmd = %f\n", diff);
    if( diff > diff_thr ) fail = 1;
    diff = fabs(p1->lat - p2->lat); //printf("diff_lat = %f\n", diff);
    if( diff > diff_thr ) fail = 1;
    diff = fabs(p1->lng - p2->lng); //printf("diff_lng = %f\n", diff);
    if( diff > diff_thr ) fail = 1;
    diff = fabs(p1->alt - p2->alt); //printf("diff_alt = %f\n", diff);
    if( diff > diff_thr ) fail = 1;

    diff = fabs(p1->frame - p2->frame); //printf("diff_frame = %f\n", diff);
    if( diff > diff_thr ) fail = 1;

    /*
    diff = fabs(p1->param1 - p2->param1); printf("diff_param1 = %f\n", diff);
    if( diff > diff_thr ) fail = 1;
    diff = fabs(p1->param2 - p2->param2); printf("diff_param2 = %f\n", diff);
    if( diff > diff_thr ) fail = 1;
    diff = fabs(p1->param3 - p2->param3); printf("diff_param3 = %f\n", diff);
    if( diff > diff_thr ) fail = 1;
    diff = fabs(p1->param4 - p2->param4); printf("diff_param4 = %f\n", diff);
    if( diff > diff_thr ) fail = 1;
    */

    return fail;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

AP_MissionArray::AP_MissionArray()
{
    m_timer.setName("AP_MissionArray.Timer");

    init();

    m_uas = NULL;
}

AP_MissionArray::~AP_MissionArray()
{
    release();

    stopTimer();

    m_uas = NULL;
}

void AP_MissionArray::init(void)
{
    m_arrWP.clear();

    m_iRW = 0;
    m_nRW = 0;
    m_status = IDLE;

    m_currWaypoint = 0;
}

void AP_MissionArray::release(void)
{
    if( m_arrWP.size() > 0 ) {
        AP_MissionItemMap::iterator it;
        AP_MissionItem *p;

        for(it=m_arrWP.begin(); it!=m_arrWP.end(); it++) {
            p = it->second;
            delete p;
            p = NULL;
        }

        m_arrWP.clear();
    }

    m_iRW = 0;
    m_nRW = 0;

    m_currWaypoint = 0;
}

int AP_MissionArray::set(AP_MissionArray *wpa)
{
    // clear old wps
    clear();

    // insert all wps
    AP_MissionItemMap::iterator it;
    AP_MissionItem *p;

    for(it=wpa->m_arrWP.begin(); it!=wpa->m_arrWP.end(); it++) {
        p = it->second;

        set(*p);
    }

    return 0;
}

int AP_MissionArray::set(AP_MissionItem &wp)
{
    AP_MissionItemMap::iterator it;

    it = m_arrWP.find(wp.idx);
    if( it != m_arrWP.end() ) {
        *(it->second) = wp;
    } else {
        AP_MissionItem *p = new AP_MissionItem;
        *p = wp;

        m_arrWP.insert(std::pair<int, AP_MissionItem*>(wp.idx, p));
    }

    return 0;
}

int AP_MissionArray::get(AP_MissionItem &wp)
{
    AP_MissionItemMap::iterator it;

    it = m_arrWP.find(wp.idx);
    if( it != m_arrWP.end() ) {
        wp = *(it->second);
    } else {
        return -1;
    }

    return 0;
}

AP_MissionItem* AP_MissionArray::get(int idx)
{
    AP_MissionItemMap::iterator it;
    AP_MissionItem *wp;

    it = m_arrWP.find(idx);
    if( it != m_arrWP.end() ) {
        wp = it->second;
    } else {
        wp = NULL;
    }

    return wp;
}

int AP_MissionArray::getAll(AP_MissionItemVector &wps)
{
    AP_MissionItemMap::iterator it;

    wps.clear();

    for(it=m_arrWP.begin(); it!=m_arrWP.end(); it++) {
        wps.push_back(it->second);
    }

    return 0;
}

AP_MissionItemMap* AP_MissionArray::getAll(void)
{
    return &m_arrWP;
}


// FIXME: need reorder other items?
int AP_MissionArray::remove(int idx)
{
    AP_MissionItemMap::iterator it;

    it = m_arrWP.find(idx);
    if( it != m_arrWP.end() ) {
        m_arrWP.erase(it);
    } else {
        return -1;
    }

    return 0;
}

int AP_MissionArray::size(void)
{
    return m_arrWP.size();
}

int AP_MissionArray::clear(void)
{
    release();

    return 0;
}


int AP_MissionArray::save(std::string fname)
{
    FILE                *fp = NULL;
    AP_MissionItem      *p;

    // open file
    fp = fopen(fname.c_str(), "wt");
    if( fp == NULL ) {
        dbg_pe("Cannot open file: %s", fname.c_str());
        return -1;
    }

    // output waypoints
    int np = m_arrWP.size();

    fprintf(fp, "#waypoint number\n");
    fprintf(fp, "%d\n", np);

    fprintf(fp, "#waypoints list\n");
    fprintf(fp, "# idx              lat                   lng"
                "                   alt         heading  MAV_CMD"
                "          Param1                  Param2"
                "                   Param3                   Param4\n");

    for(int i=0; i<np; i++) {
        p = m_arrWP[i];

        fprintf(fp, "%4d %24.16f %24.16f %12.3f %12.3f %6d %24.16f %24.16f %24.16f %24.16f\n",
                p->idx,
                p->lat, p->lng, p->alt,
                p->heading,
                p->cmd,
                p->param1, p->param2, p->param3, p->param4);
    }

    // close file
    fclose(fp);

    return 0;
}

int AP_MissionArray::load(std::string fname)
{
    FILE                        *fp = NULL;

    char                        *buf;
    std::string                 _b;
    int                         max_line_size;
    int                         s, idx, n;

    // open file
    fp = fopen(fname.c_str(), "rt");
    if( fp == NULL ) {
        dbg_pe("Cannot open file: %s", fname.c_str());
        return -1;
    }

    // free old contents
    clear();

    // alloc memory buffer
    max_line_size = 1024;
    buf = new char[max_line_size];

    s = 0;
    idx = 0;

    while(!feof(fp)) {
        // read a line
        if( NULL == fgets(buf, max_line_size, fp) )
            break;

        // remove blank & CR
        _b = trim(buf);

        if( _b.size() < 1 )
            continue;

        // skip comment
        if( _b[0] == '#' || _b[0] == ':' )
            continue;

        if( s == 0 ) {
            // read wp number
            sscanf(_b.c_str(), "%d", &n);
            s = 1;
        } else {
            StringArray sa = split_text(_b, " ");
            StringArray sa2;

            for(int i=0; i<sa.size(); i++) {
                std::string ss = trim(sa[i]);
                if( ss.size() > 0 ) sa2.push_back(ss);
            }

            int nv = sa2.size();
            if( nv < 5 ) {
                dbg_pe("Input file format error!");
                goto LOAD_ERR;
            }


            // set mission item
            AP_MissionItem w;

            w.idx       = str_to_int(sa2[0]);
            w.lat       = str_to_float(sa2[1]);
            w.lng       = str_to_float(sa2[2]);
            w.alt       = str_to_float(sa2[3]);
            w.heading   = str_to_float(sa2[4]);

            if( nv > 5 ) w.cmd = (MAV_CMD) str_to_int(sa2[5]);
            else         w.cmd = MAV_CMD_NAV_WAYPOINT;

            if( nv >= 9 ) {
                w.param1 = str_to_double(sa2[6]);
                w.param2 = str_to_double(sa2[7]);
                w.param3 = str_to_double(sa2[8]);
                w.param4 = str_to_double(sa2[9]);
            }

            // insert to map
            set(w);

            /*
            printf("%4d %24.16f %24.16f %12.3f %12.3f %d\n",
                    w.idx,
                    w.lat, w.lng, w.alt,
                    w.heading,
                    w.cmd);
            */

            idx ++;
        }
    }

LOAD_ERR:
    // free buffer
    delete [] buf;

    // close file
    fclose(fp);

    return 0;
}


int AP_MissionArray::writeWaypoints(void)
{
    // check waypoint number
    if( size() <= 1 ) {
        dbg_pe("Waypoints number (%d) is not enough!", size());
        return -1;
    }

    // clear confirm flag
    AP_MissionItemMap::iterator it;
    AP_MissionItem *p;

    for(it=m_arrWP.begin(); it!=m_arrWP.end(); it++) {
        p = it->second;
        p->writeConfirmed = 0;
    }

    m_wpList.clear();
    for(int i=0; i<m_arrWP.size(); i++) m_wpList.push_back(i);

    // send waypoints number
    writeWaypointsNum();

    return 0;
}

int AP_MissionArray::readWaypoints(void)
{
    m_status = READING_NUM;
    m_nReadTry = 4;

    readWaypointsNum();

    return 0;
}

int AP_MissionArray::writeWaypointsNum(void)
{
    // set numbers & status
    m_status = WRITTING_NUM;
    m_nRW = size();

    // send waypoint count
    mavlink_mission_count_t mc;
    mavlink_message_t msg;

    mc.count            = m_nRW;                           // include home wp
    mc.target_system    = m_uas->ID;
    mc.target_component = m_uas->compID;

    mavlink_msg_mission_count_encode(m_uas->gcsID, m_uas->gcsCompID, &msg, &mc);
    m_uas->sendMavlinkMsg(&msg);

    dbg_pi("writeWaypointsNum: %d", m_nRW);

    m_tLast = tm_get_millis();

    return 0;
}


int AP_MissionArray::readWaypointsNum(void)
{
    // clear old contents
    // FIXME: better way?
    clear();

    return _readWaypointsNum();
}


int AP_MissionArray::clearWaypoints(void)
{
    mavlink_message_t msg;
    mavlink_mission_clear_all_t mca;

    m_status = CLEAR_WP;

    mca.target_system = m_uas->ID;
    mca.target_component = m_uas->compID;

    mavlink_msg_mission_clear_all_encode(m_uas->gcsID, m_uas->gcsCompID, &msg, &mca);
    m_uas->sendMavlinkMsg(&msg);

    dbg_pi("clearWaypoints");

    m_tLast = tm_get_millis();

    return 0;
}

int AP_MissionArray::setCurrentWaypoint(int idx)
{
    mavlink_message_t msg;
    mavlink_mission_set_current_t msc;

    // check index range
    if( idx >= size() || idx < 0 ) {
        dbg_pe("Given waypoint index (%d) out of rang [%d~%d].\n", idx, 1, size());
        return -1;
    }

    m_currWaypoint = idx;
    m_uas->currMission = idx;

    msc.seq = idx;
    msc.target_system = m_uas->ID;
    msc.target_component = m_uas->compID;

    mavlink_msg_mission_set_current_encode(m_uas->gcsID, m_uas->gcsCompID, &msg, &msc);
    m_uas->sendMavlinkMsg(&msg);

    dbg_pi("setCurrentWaypoint: [%d]", idx);

    m_tLast = tm_get_millis();

    return 0;
}

int AP_MissionArray::_readWaypointsNum(void)
{
    m_iRW = 0;

    // require mission list
    mavlink_mission_request_list_t mrl;
    mavlink_message_t msg;

    mrl.target_system = m_uas->ID;
    mrl.target_component = m_uas->compID;

    mavlink_msg_mission_request_list_encode(m_uas->gcsID, m_uas->gcsCompID,
                                            &msg, &mrl);
    m_uas->sendMavlinkMsg(&msg);

    m_tLast = tm_get_millis();

    return 0;
}

int AP_MissionArray::_readWaypoint(int idx)
{
    // require given wp
    mavlink_mission_request_t mr;
    mavlink_message_t msgSend;

    mr.seq = idx;
    mr.target_system = m_uas->ID;
    mr.target_component = m_uas->compID;

    mavlink_msg_mission_request_encode(m_uas->gcsID, m_uas->gcsCompID,
                                       &msgSend, &mr);
    m_uas->sendMavlinkMsg(&msgSend);

    m_tLast = tm_get_millis();

    return 0;
}

int AP_MissionArray::_writeWaypoint(int idx)
{
    // get waypoint
    AP_MissionItem *p = get(idx);
    if( p == NULL ) {
        dbg_pe("Given waypoint[%d] not exist!\n", idx);
        return -1;
    }

    // send waypoint item to MAV
    mavlink_mission_item_t mmi;
    mavlink_message_t msgSend;

    p->to_mission_item(&mmi);
    mmi.seq = idx;
    mmi.target_system = m_uas->ID;
    mmi.target_component = m_uas->compID;

    dbg_pi("send mission[%3d/%3d] cmd = %d, x,y,z = %f %f %f\n",
           idx, size(), mmi.command,
           mmi.x, mmi.y, mmi.z);
    dbg_pi("                  p1, p2, p3, p4 = %f %f %f %f\n",
           mmi.param1, mmi.param2, mmi.param3, mmi.param4);
    dbg_pi("                  frame = %d, current = %d, autocontinue = %d\n",
           mmi.frame, mmi.current, mmi.autocontinue);

    mavlink_msg_mission_item_encode(m_uas->gcsID, m_uas->gcsCompID, &msgSend, &mmi);
    m_uas->sendMavlinkMsg(&msgSend);

    m_tLast = tm_get_millis();

    return 0;
}

int AP_MissionArray::_sendWritePartialList(int idx)
{
    mavlink_mission_write_partial_list_t pl;
    mavlink_message_t msgSend;

    pl.target_system = m_uas->ID;
    pl.target_component = m_uas->compID;
    pl.start_index = idx;
    pl.end_index = idx;

    dbg_pi("send write partial list: idx = %d", idx);

    mavlink_msg_mission_write_partial_list_encode(m_uas->gcsID, m_uas->gcsCompID, &msgSend, &pl);
    m_uas->sendMavlinkMsg(&msgSend);

    m_tLast = tm_get_millis();

    return 0;
}

int AP_MissionArray::timerFunction(void *arg)
{
    if( m_status == IDLE ) return 0;

    // get current time & dt
    uint64_t dt, dt_thr = 1000,
             tnow = tm_get_millis();
    if( tnow < m_tLast ) return 0;
    dt = tnow - m_tLast;

    // if do not receive any mission count message then assume the mission
    //  number is zero
    if( m_status == READING_NUM && dt > dt_thr ) {
        if( m_nReadTry > 0 ) {
            m_nReadTry --;          
            _readWaypointsNum();
        } else {
            dbg_pw("MAV really dont have any mission!\n");
            m_tLast = tm_get_millis();
            m_status = IDLE;
        }

        return 0;
    }

    // if stop receiving waypoint then resend request msg again
    if( m_status == READING_ITEM && dt > dt_thr ) {
        if( m_iRW < m_nRW - 1 ) {
            // require current wp
            _readWaypoint(m_iRW);
        } else {
            // finished reading
            mavlink_mission_ack_t ma;
            mavlink_message_t msgSend;

            ma.type = MAV_MISSION_ACCEPTED;
            mavlink_msg_mission_ack_encode(m_uas->gcsID, m_uas->gcsCompID, &msgSend, &ma);

            m_uas->sendMavlinkMsg(&msgSend);

            m_status = IDLE;
            m_tLast = tm_get_millis();
        }

        return 0;
    }


    // if write waypoint, but receiving any request then send mission count again
    if( m_status == WRITTING_NUM && dt > dt_thr ) {
        writeWaypointsNum();

        return 0;
    }

    if( m_status == WRITTING_ITEM && dt > dt_thr ) {
        _sendWritePartialList(m_iRW);

        return 0;
    }


    // if confirm uploaded mission, then resend the request
    if( m_status == WRITTING_CONFIRM_ITEM && dt > dt_thr ) {
        if( m_iRW < m_nRW - 1 ) {
            // require next wp
            _readWaypoint(m_iRW);
        } else {
            // check received items are correct
            AP_MissionItemMap::iterator it;
            AP_MissionItem *p;
            int fail = 0;

            for(it=m_arrWP.begin(); it!=m_arrWP.end(); it++) {
                p = it->second;

                if( p->idx > 0 && !p->writeConfirmed ) {
                    fail = 1;
                }
            }


            if( fail ) {
                // some mission items are error
                m_wpList.clear();
                for(int i=0; i<size(); i++) {
                    p = get(i);
                    if( p && !p->writeConfirmed ) {
                        m_wpList.push_back(p->idx);
                    }
                }

                // re-write mission items again
                m_iRW = m_wpList.front();
                m_wpList.pop_front();
                m_status = WRITTING_ITEM;
                _sendWritePartialList(m_iRW);
            } else {
                // finished mession confirm
                m_status = IDLE;
                m_tLast = tm_get_millis();

                // set to mission 1
                setCurrentWaypoint(1);
            }
        }

        return 0;
    }

    if( m_status == CLEAR_WP && dt > dt_thr ) {
        clearWaypoints();

        return 0;
    }

    return 0;
}

int AP_MissionArray::parseMavlinkMsg(mavlink_message_t *msg)
{
    int ret = 0;

    switch(msg->msgid) {
    case MAVLINK_MSG_ID_MISSION_ITEM:               // 39
    {
        ret = 1;

        mavlink_mission_item_t mi;
        mavlink_msg_mission_item_decode(msg, &mi);

        dbg_pi("recv mission[%3d] cmd = %d, x,y,z = %f %f %f\n",
               mi.seq, mi.command, mi.x, mi.y, mi.z);
        dbg_pi("                  p1, p2, p3, p4 = %f %f %f %f\n",
               mi.param1, mi.param2, mi.param3, mi.param4);
        dbg_pi("                  frame = %d, current = %d, autocontinue = %d\n",
               mi.frame, mi.current, mi.autocontinue);

        AP_MissionItem wp;
        wp.from_mission_item(&mi);

        m_tLast = tm_get_millis();

        if( m_iRW == mi.seq ) {
            if( m_status == READING_ITEM )
                set(wp);
            else if( m_status == WRITTING_CONFIRM_ITEM ) {
                AP_MissionItem *p = get(wp.idx);

                // check uploaded waypoint is correct or not
                if( wp.idx>0 && wp.compare(p) ) {
                    m_wpList.push_back(wp.idx);
                    p->writeConfirmed = 0;
                } else {
                    p->writeConfirmed = 1;
                }
            }

            if( mi.seq >= m_nRW - 1 ) {
                // send ACK to MAV
                mavlink_mission_ack_t ma;
                mavlink_message_t msgSend;

                ma.type = MAV_MISSION_ACCEPTED;
                mavlink_msg_mission_ack_encode(m_uas->gcsID, m_uas->gcsCompID, &msgSend, &ma);
                m_uas->sendMavlinkMsg(&msgSend);

                if( m_status == READING_ITEM ) {
                    dbg_pi("recv mission finished!\n");

                    m_status = IDLE;
                } else if ( m_status == WRITTING_CONFIRM_ITEM ) {
                    if( m_wpList.size() > 0 ) {
                        dbg_pe("Wrote mission error! resend them again");

                        writeWaypoints();
                    } else {
                        dbg_pi("confirm mission finished!\n");

                        m_status = IDLE;

                        setCurrentWaypoint(1);
                    }
                }
            } else {
                // require next wp
                m_iRW = m_wpList.front();
                m_wpList.pop_front();

                _readWaypoint(m_iRW);
            }
        } else {
            // require current wp again
            _readWaypoint(m_iRW);
        }

        break;
    }

    case MAVLINK_MSG_ID_MISSION_REQUEST:            // 40
    {
        ret = 1;

        mavlink_mission_request_t mr;
        mavlink_msg_mission_request_decode(msg, &mr);

        // check & get waypoint item
        dbg_pi("req mission [%3d/%3d]\n", mr.seq, size());

        m_tLast = tm_get_millis();

        if( m_status == WRITTING_NUM ) {
            m_status = WRITTING_ITEM;

            m_iRW = m_wpList.front();
            m_wpList.pop_front();
            _sendWritePartialList(m_iRW);

            break;
        } else if( m_status == WRITTING_ITEM ) {
            // send waypoint item to MAV
            _writeWaypoint(mr.seq);
        }

        break;
    }

    case MAVLINK_MSG_ID_MISSION_CURRENT:            // 42
    {
        ret = 1;

        mavlink_mission_current_t mc;
        mavlink_msg_mission_current_decode(msg, &mc);

        m_currWaypoint = mc.seq;
        m_uas->currMission = mc.seq;

        break;
    }

    case MAVLINK_MSG_ID_MISSION_COUNT:              // 44
    {
        ret = 1;

        mavlink_mission_count_t mc;
        mavlink_msg_mission_count_decode(msg, &mc);

        m_tLast = tm_get_millis();

        dbg_pi("recv missions number: %d, m_status = %d\n", mc.count, m_status);

        // check sent waypoint number is correct or not
        if( m_status == WRITTING_NUM ) {
            if( mc.count == size() ) {
                m_status = WRITTING_ITEM;

                m_iRW = m_wpList.front();
                m_wpList.pop_front();
                _sendWritePartialList(m_iRW);
            } else {
                dbg_pe("upload waypoints number incorrect %d\n", mc.count);
                writeWaypointsNum();
            }

            break;
        }

        if( m_status == READING_NUM ) {
            m_nRW = mc.count;

            if( mc.count <= 0 ) {
                // set reading status to 3 (finished)
                m_status = IDLE;
                break;
            }

            m_wpList.clear();
            for(int i=0; i<m_nRW; i++) m_wpList.push_back(i);

            // set rw index to first wp
            m_iRW = m_wpList.front();
            m_wpList.pop_front();

            m_nReadTry = 0;
            _readWaypoint(m_iRW);

            m_status = READING_ITEM;
        }

        break;
    }

    case MAVLINK_MSG_ID_MISSION_ACK:                // 47
    {
        ret = 1;

        mavlink_mission_ack_t ma;
        mavlink_msg_mission_ack_decode(msg, &ma);

        dbg_pi("mission_ack: %d\n", ma.type);

        m_tLast = tm_get_millis();

        // if current state is waypoint writting
        if( m_status == WRITTING_ITEM ) {
            // success
            if( ma.type == 0 ) {
                if( m_wpList.size() > 0 ) {
                    // next mission item
                    m_iRW = m_wpList.front();
                    m_wpList.pop_front();

                    _sendWritePartialList(m_iRW);
                } else {
                    // confirm all mission items
                    m_status = WRITTING_CONFIRM_ITEM;

                    m_wpList.clear();
                    for(int i=0; i<size(); i++) m_wpList.push_back(i);

                    m_iRW = m_wpList.front();
                    m_wpList.pop_front();

                    _readWaypoint(m_iRW);
                }
            } else {
                // if failed send wp number again
                _sendWritePartialList(m_iRW);
            }

            break;
        }

        // current state is waypoint clear
        if( m_status == CLEAR_WP ) {
            if( ma.type == 0 ) {
                m_status = IDLE;
            } else {
                clearWaypoints();
            }

            break;
        }

        break;
    }

    } // end of switch(msg->msgid)

    return ret;
}

int AP_MissionArray::startTimer(void)
{
    if( !m_timer.isRunning() ) {
        m_timer.setPeriodicInterval(200);
        m_timer.start(AP_ParamArray_timerFunc, this);
    }

    return 0;
}

int AP_MissionArray::stopTimer(void)
{
    if( m_timer.isRunning() ) {
        m_timer.stop();
    }

    return 0;
}
